package com.ntk.login;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public interface ILoginView {
    void showLoad();
    void dismissLoad();
    void nextPage(String phone, String nik);
    void returnNik();
    void showError(String error);
}
