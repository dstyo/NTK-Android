package com.ntk.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.base.BaseActivity;
import com.ntk.data.ApiService;
import com.ntk.utils.CommonUtils;
import com.ntk.verification.VerificationActivity;
import com.rengwuxian.materialedittext.MaterialEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.26.02
 */
public class LoginActivity extends BaseActivity implements ILoginView {

    @BindView(R.id.input_nik)
    MaterialEditText inputNik;
    @BindView(R.id.text_terms)
    TextView textTerms;
    @BindView(R.id.text_help)
    TextView textHelp;
    @BindView(R.id.layout_nik)
    LinearLayout layoutNik;
    @BindView(R.id.input_phone)
    MaterialEditText inputPhone;
    @BindView(R.id.text_have_code)
    TextView textHaveCode;
    @BindView(R.id.layout_phone)
    LinearLayout layoutPhone;
    @BindView(R.id.button_login)
    MaterialRippleLayout buttonLogin;
    @BindView(R.id.cardView)
    CardView cardView;

    ProgressDialog progressDialog;

    private LoginPresenter loginPresenter;
    @Inject
    ApiService apiService;

    String nik, phone, deviceId;


    @Override
    protected int getActivityView() {
        return R.layout.activity_login_nik;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        ((QualityFeedbackApp) getApplication())
                .getComponent()
                .inject(this);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            deviceId = bundle.getString("gcm");

        }
        setupPresenter();
        changeColorSystemBar(R.color.background_grey_quality_survey);
        layoutNik.setVisibility(View.VISIBLE);
        layoutPhone.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        disable();
        permissionSMS();
        inputNik.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (inputNik.length() == 5) {
                    loginPresenter.checkNik(inputNik.getText().toString());
                    nik = inputNik.getText().toString();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputNik.length() < 5) {
                    inputNik.requestFocus();
                }
            }
        });

    }

    @OnClick(R.id.button_login)
    public void onClick() {
        if (inputPhone.length() == 0) {
            inputPhone.setError(getResources().getString(R.string.login_null));
        } else {
            loginPresenter.reqCode(inputNik.getText().toString(), inputPhone.getText().toString());
        }
    }

    @Override
    public void showLoad() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.dialog_progress));
        progressDialog.show();
    }

    @Override
    public void dismissLoad() {
        progressDialog.dismiss();
    }

    @Override
    public void nextPage(String phone, String nik) {
        Intent intent = new Intent(this, VerificationActivity.class);
        intent.putExtra("nik", nik);
        intent.putExtra("gcm", deviceId);
        startActivity(intent);
        finish();
    }

    @Override
    public void returnNik() {
        if (layoutNik.getVisibility() == View.VISIBLE) {
            layoutNik.setVisibility(View.GONE);
            layoutPhone.setVisibility(View.VISIBLE);
            inputPhone.requestFocus();
            enable();
        }
    }

    public void disable() {
        buttonLogin.setEnabled(false);
        cardView.setCardBackgroundColor(CommonUtils.getColor(this, R.color.text_grey_quality_survey));
    }

    public void enable() {
        buttonLogin.setEnabled(true);
        cardView.setCardBackgroundColor(CommonUtils.getColor(this, R.color.nutricia_red));
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(error)
                .show();
    }

    private void setupPresenter() {
        if (loginPresenter == null) {
            loginPresenter = new LoginPresenter(this, apiService);
        }
    }

    @OnClick(R.id.text_have_code)
    public void onhaveCode() {
        nextPage("", nik);
    }

    public void permissionSMS(){
        if (ActivityCompat.checkSelfPermission(Activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                    123);
        }

        if (ActivityCompat.checkSelfPermission(Activity, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Activity, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS},
                    123);
        }

        if (ActivityCompat.checkSelfPermission(Activity, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Activity, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_SMS},
                    123);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 123:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    CommonUtils.showToast(Activity, "Some Permission is Denied");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
