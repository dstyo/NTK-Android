package com.ntk.login;

import com.ntk.data.ApiService;
import com.ntk.data.model.InputNik;
import com.ntk.data.model.ReturnString;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class LoginPresenter {
    private ILoginView iLoginView;
    private ApiService apiService;

    public LoginPresenter(ILoginView iLoginView, ApiService apiService){
        this.iLoginView = iLoginView;
        this.apiService = apiService;
    }

    public void checkNik(String Nik){
        iLoginView.showLoad();
        InputNik inputNik = new InputNik();
        inputNik.setNik(Nik);
        apiService.postNik(inputNik)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iLoginView.showError(e.getMessage());
                        iLoginView.dismissLoad();
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getData().equals("LOGIN")) {
                            iLoginView.returnNik();
                        }
                        else {
                            iLoginView.showError(returnString.getData());
                        }
                        iLoginView.dismissLoad();

                    }
                });
    }

    public void reqCode(final String Nik, final String phone){
        iLoginView.showLoad();
        InputNik inputNik = new InputNik();
        inputNik.setNik(Nik);
        inputNik.setPhone(phone);
        apiService.requestCode(inputNik, "no")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iLoginView.showError(e.getMessage());
                        iLoginView.dismissLoad();
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getMeta().getCode()==200) {
                            iLoginView.nextPage(phone, Nik);
                        }
                        else {
                            iLoginView.showError(returnString.getMeta().getMessage());
                        }
                        iLoginView.dismissLoad();

                    }
                });
    }

}
