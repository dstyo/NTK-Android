package com.ntk.setting;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bruce.pickerview.LoopView;
import com.ntk.R;
import com.ntk.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dycode on 20/10/2016.
 */

public class SettingActivity extends BaseActivity {
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.text_Save)
    TextView textSave;
    @BindView(R.id.layout_about)
    LinearLayout layoutAbout;
    @BindView(R.id.layout_faq)
    LinearLayout layoutFaq;
    @BindView(R.id.layout_language)
    LinearLayout layoutLanguage;
    @BindView(R.id.notif)
    Switch notif;
    @BindView(R.id.layout_notification)
    LinearLayout layoutNotification;

    Locale myLocale;
    List<String> list = new ArrayList<>();
    List<String> code = new ArrayList<>();
    @BindView(R.id.text_sub)
    TextView textSub;
    LoopView rvProduct;
    int sub = 0;
    @Override
    protected int getActivityView() {
        return R.layout.activitys_setting;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        String[] string_sub, string_code_sub;
        string_sub = getResources().getStringArray(R.array.subtitle);
        string_code_sub = getResources().getStringArray(R.array.code_subtitle);

        for (int i = 0; i < string_sub.length; i++) {
            list.add(string_sub[i]);
            code.add(string_code_sub[i]);
        }
        Locale current = getResources().getConfiguration().locale;

        for (int i = 0; i < code.size(); i++){
            if (code.get(i).equals(current.getLanguage())){
                textSub.setText(list.get(i));
            }
        }
    }

    @OnClick(R.id.layout_language)
    public void onLanguage() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_subtitle);
        dialog.show();

        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM | Gravity.LEFT | Gravity.RIGHT;
        dialog.getWindow().setAttributes(params);

        rvProduct = ButterKnife.findById(dialog, R.id.rv_subtitle);
        TextView textCancel = ButterKnife.findById(dialog, R.id.text_cancel);
        TextView textConfirm = ButterKnife.findById(dialog, R.id.text_confirm);
        textCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        textConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                textSub.setText(list.get(rvProduct.getSelectedItem()));
                sub = 1;
            }
        });
        rvProduct.setNotLoop();
        rvProduct.setTextSize(16);
        rvProduct.setArrayList((ArrayList) list);
        rvProduct.setInitPosition(0);
        dialog.show();
    }

    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.text_Save)
    public void onSave() {
        if (sub == 1){
            setLocale(code.get(rvProduct.getSelectedItem()));
        }

    }
}
