package com.ntk;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.ntk.module.ApplicationComponent;
import com.ntk.module.ApplicationModule;
import com.ntk.module.DaggerApplicationComponent;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class QualityFeedbackApp extends Application {

    private static Context context;

    public static Context getAppContext() {
        return QualityFeedbackApp.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RxPaparazzo.register(this);
        setupCrashlytics();
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        QualityFeedbackApp.context = getApplicationContext();
    }
    public static QualityFeedbackApp get(Context context){
        return (QualityFeedbackApp) context.getApplicationContext();
    }


    private void setupCrashlytics(){
        Fabric.with(this, new Crashlytics());
    }

    private ApplicationComponent mComponent;

    public ApplicationComponent getComponent() {
        return mComponent;
    }

}
