package com.ntk.gcm;

/**
 * Created by Dean Nurchusnul on 20/09/2016.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}