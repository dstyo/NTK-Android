package com.ntk.module;

import android.app.Application;

import dagger.Module;

/**
 * Created by Dean Nurchusnul on 02/08/2016.
 */
@Module
public class DemoApplication extends Application {
    private ApplicationComponent mComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
    public ApplicationComponent getComponent() {
        return mComponent;
    }
}