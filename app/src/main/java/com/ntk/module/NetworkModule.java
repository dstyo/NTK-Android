package com.ntk.module;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ntk.constant.UrlConstant;
import com.ntk.data.ApiService;
import com.ntk.data.retrofit.AppRequestHeaderInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmObject;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dean Nurchusnul on 02/08/2016.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Gson providesGson() {
        return new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

    }
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public AppRequestHeaderInterceptor provideAppRequestHeaderInterceptor() {
        return new AppRequestHeaderInterceptor();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public HttpLoggingInterceptor providesHttpLoggingInterceptor(Context context) {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public okhttp3.OkHttpClient providesOkHttpClient3(
            HttpLoggingInterceptor httpLoggingInterceptor,
            AppRequestHeaderInterceptor appRequestHeaderInterceptor) {
        return new okhttp3.OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(appRequestHeaderInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Retrofit.Builder providesRetrofitBuilder(
            okhttp3.OkHttpClient okHttpClient,
            Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public Retrofit providesMagicButtonRetrofit(
            Retrofit.Builder builder) {
        return builder.baseUrl(UrlConstant.ROOT)
                .build();
    }
    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ApiService apiService(
            Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}
