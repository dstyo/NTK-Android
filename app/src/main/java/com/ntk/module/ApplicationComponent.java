package com.ntk.module;

import android.app.Application;
import android.content.Context;

import com.ntk.NewsDetail.NewsDetailActivity;
import com.ntk.home.HomeActivity;
import com.ntk.home.fragment.HistoryFragment;
import com.ntk.home.fragment.MainFragment;
import com.ntk.home.fragment.NewsFragment;
import com.ntk.home.fragment.ProfileFragment;
import com.ntk.login.LoginActivity;
import com.ntk.verification.VerificationActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Dean Nurchusnul on 02/08/2016.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    Context context();

    Application application();

    void inject(DemoApplication app);

    void inject(LoginActivity activity);

    void inject(VerificationActivity activity);

    void inject(HomeActivity activity);

    void inject(MainFragment mainFragment);

    void inject(ProfileFragment profileFragment);

    void inject(NewsFragment newsFragment);

    void inject(NewsDetailActivity newsDetailActivity);

    void inject(HistoryFragment historyFragment);
}