package com.ntk.data.model.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
@Generated("org.jsonschema2pojo")
public class Data {

    @SerializedName("brands")
    @Expose
    private List<Brand> brands = new ArrayList<Brand>();
    @SerializedName("products")
    @Expose
    private List<Product> products = new ArrayList<Product>();
    @SerializedName("parts")
    @Expose
    private List<Part> parts = new ArrayList<Part>();
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = new ArrayList<Condition>();
    @SerializedName("trades")
    @Expose
    private List<Trade> trades = new ArrayList<Trade>();
    @SerializedName("vendorTypes")
    @Expose
    private List<VendorType> vendorTypes = new ArrayList<VendorType>();
    @SerializedName("codeProduction")
    @Expose
    private List<CodeProduction> codeProduction = new ArrayList<CodeProduction>();

    /**
     *
     * @return
     * The brands
     */
    public List<Brand> getBrands() {
        return brands;
    }

    /**
     *
     * @param brands
     * The brands
     */
    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    /**
     *
     * @return
     * The products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     *
     * @return
     * The parts
     */
    public List<Part> getParts() {
        return parts;
    }

    /**
     *
     * @param parts
     * The parts
     */
    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    /**
     *
     * @return
     * The conditions
     */
    public List<Condition> getConditions() {
        return conditions;
    }

    /**
     *
     * @param conditions
     * The conditions
     */
    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    /**
     *
     * @return
     * The trades
     */
    public List<Trade> getTrades() {
        return trades;
    }

    /**
     *
     * @param trades
     * The trades
     */
    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }

    /**
     *
     * @return
     * The vendorTypes
     */
    public List<VendorType> getVendorTypes() {
        return vendorTypes;
    }

    /**
     *
     * @param vendorTypes
     * The vendorTypes
     */
    public void setVendorTypes(List<VendorType> vendorTypes) {
        this.vendorTypes = vendorTypes;
    }

    /**
     *
     * @return
     * The codeProduction
     */
    public List<CodeProduction> getCodeProduction() {
        return codeProduction;
    }

    /**
     *
     * @param codeProduction
     * The codeProduction
     */
    public void setCodeProduction(List<CodeProduction> codeProduction) {
        this.codeProduction = codeProduction;
    }

}