package com.ntk.data.model;

/**
 * Created by Dycode on 19/10/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class AddComment {

    @SerializedName("newsId")
    @Expose
    private Integer newsId;
    @SerializedName("comment")
    @Expose
    private String comment;

    /**
     *
     * @return
     * The newsId
     */
    public Integer getNewsId() {
        return newsId;
    }

    /**
     *
     * @param newsId
     * The newsId
     */
    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

}