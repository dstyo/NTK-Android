package com.ntk.data.model.history;

/**
 * Created by Dycode on 26/10/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Condition {

    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("part")
    @Expose
    private String part;
    @SerializedName("description")
    @Expose
    private String description;

    /**
     *
     * @return
     * The condition
     */
    public String getCondition() {
        return condition;
    }

    /**
     *
     * @param condition
     * The condition
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     *
     * @return
     * The part
     */
    public String getPart() {
        return part;
    }

    /**
     *
     * @param part
     * The part
     */
    public void setPart(String part) {
        this.part = part;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}