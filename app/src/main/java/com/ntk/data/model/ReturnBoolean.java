package com.ntk.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by Dycode on 11/11/2016.
 */
@Generated("org.jsonschema2pojo")
public class ReturnBoolean {
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private Boolean data;

    /**
     * @return The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The data
     */
    public Boolean getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Boolean data) {
        this.data = data;
    }
}
