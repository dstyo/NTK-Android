package com.ntk.data.model;

/**
 * Created by Dycode on 19/10/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ReturnAddComment {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private DataComment data;

    /**
     * @return The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * @return The data
     */
    public DataComment getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(DataComment data) {
        this.data = data;
    }
}