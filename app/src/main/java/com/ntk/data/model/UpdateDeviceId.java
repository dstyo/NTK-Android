package com.ntk.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by Dean Nurchusnul on 20/09/2016.
 */
@Generated("org.jsonschema2pojo")
public class UpdateDeviceId {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("client")
    @Expose
    private Integer client;

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     * The client
     */
    public Integer getClient() {
        return client;
    }

    /**
     *
     * @param client
     * The client
     */
    public void setClient(Integer client) {
        this.client = client;
    }

}