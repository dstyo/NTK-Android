package com.ntk.data.model;

/**
 * Created by Dycode on 18/10/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class UserData {

    @SerializedName("nik")
    @Expose
    private String nik;
    @SerializedName("fullname")
    @Expose
    private String fullname;

    /**
     *
     * @return
     * The nik
     */
    public String getNik() {
        return nik;
    }

    /**
     *
     * @param nik
     * The nik
     */
    public void setNik(String nik) {
        this.nik = nik;
    }

    /**
     *
     * @return
     * The fullname
     */
    public String getFullname() {
        return fullname;
    }

    /**
     *
     * @param fullname
     * The fullname
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}