package com.ntk.data.model.history;

/**
 * Created by Dycode on 26/10/2016.
 */

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Datum {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("date_")
    @Expose
    private String date_;
    @SerializedName("history")
    @Expose
    private List<History> history = new ArrayList<History>();

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The dateA
     */
    public String getDate_() {
        return date_;
    }

    /**
     *
     * @param dateA
     * The date_a
     */
    public void setDate_(String date_) {
        this.date_ = date_;
    }

    /**
     *
     * @return
     * The history
     */
    public List<History> getHistory() {
        return history;
    }

    /**
     *
     * @param history
     * The history
     */
    public void setHistory(List<History> history) {
        this.history = history;
    }

}