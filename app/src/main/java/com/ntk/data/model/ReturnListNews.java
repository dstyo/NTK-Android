package com.ntk.data.model;

/**
 * Created by Dycode on 17/10/2016.
 */
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ReturnListNews {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private List<ListNews> data = new ArrayList<ListNews>();

    /**
     *
     * @return
     * The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     *
     * @param meta
     * The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     *
     * @return
     * The data
     */
    public List<ListNews> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<ListNews> data) {
        this.data = data;
    }

}
