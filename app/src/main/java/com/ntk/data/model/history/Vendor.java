package com.ntk.data.model.history;

/**
 * Created by Dycode on 26/10/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Vendor {

    @SerializedName("vendorName")
    @Expose
    private String vendorName;
    @SerializedName("vendorAddress")
    @Expose
    private String vendorAddress;
    @SerializedName("vendorCity")
    @Expose
    private String vendorCity;
    @SerializedName("vendorProvince")
    @Expose
    private String vendorProvince;
    @SerializedName("vendorType")
    @Expose
    private String vendorType;
    @SerializedName("latitude")
    @Expose
    private Integer latitude;
    @SerializedName("longitude")
    @Expose
    private Integer longitude;

    /**
     *
     * @return
     * The vendorName
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     *
     * @param vendorName
     * The vendorName
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     *
     * @return
     * The vendorAddress
     */
    public String getVendorAddress() {
        return vendorAddress;
    }

    /**
     *
     * @param vendorAddress
     * The vendorAddress
     */
    public void setVendorAddress(String vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    /**
     *
     * @return
     * The vendorCity
     */
    public String getVendorCity() {
        return vendorCity;
    }

    /**
     *
     * @param vendorCity
     * The vendorCity
     */
    public void setVendorCity(String vendorCity) {
        this.vendorCity = vendorCity;
    }

    /**
     *
     * @return
     * The vendorProvince
     */
    public String getVendorProvince() {
        return vendorProvince;
    }

    /**
     *
     * @param vendorProvince
     * The vendorProvince
     */
    public void setVendorProvince(String vendorProvince) {
        this.vendorProvince = vendorProvince;
    }

    /**
     *
     * @return
     * The vendorType
     */
    public String getVendorType() {
        return vendorType;
    }

    /**
     *
     * @param vendorType
     * The vendorType
     */
    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    /**
     *
     * @return
     * The latitude
     */
    public Integer getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public Integer getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

}