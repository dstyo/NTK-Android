package com.ntk.data.model.history;

/**
 * Created by Dycode on 26/10/2016.
 */

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class History {
    private Boolean selected = false;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("productionCode")
    @Expose
    private String productionCode;
    @SerializedName("isGood")
    @Expose
    private Boolean isGood;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("vendor")
    @Expose
    private Vendor vendor;
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = new ArrayList<Condition>();
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("timePost")
    @Expose
    private String timePost;
    @SerializedName("description")
    @Expose
    private String description;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The product
     */
    public String getProduct() {
        return product;
    }

    /**
     *
     * @param product
     * The product
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     *
     * @return
     * The brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     *
     * @param brand
     * The brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     *
     * @return
     * The sku
     */
    public String getSku() {
        return sku;
    }

    /**
     *
     * @param sku
     * The sku
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     *
     * @return
     * The productionCode
     */
    public String getProductionCode() {
        return productionCode;
    }

    /**
     *
     * @param productionCode
     * The productionCode
     */
    public void setProductionCode(String productionCode) {
        this.productionCode = productionCode;
    }

    /**
     *
     * @return
     * The isGood
     */
    public Boolean getIsGood() {
        return isGood;
    }

    /**
     *
     * @param isGood
     * The isGood
     */
    public void setIsGood(Boolean isGood) {
        this.isGood = isGood;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     *
     * @param vendor
     * The vendor
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    /**
     *
     * @return
     * The conditions
     */
    public List<Condition> getConditions() {
        return conditions;
    }

    /**
     *
     * @param conditions
     * The conditions
     */
    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    /**
     *
     * @return
     * The time
     */
    public String getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The timePost
     */
    public String getTimePost() {
        return timePost;
    }

    /**
     *
     * @param timePost
     * The timePost
     */
    public void setTimePost(String timePost) {
        this.timePost = timePost;
    }
    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSelected(){
        return selected;
    }
    public void setSelected(Boolean selected){
        this.selected = selected;
    }

}