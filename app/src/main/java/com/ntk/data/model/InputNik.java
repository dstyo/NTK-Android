package com.ntk.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
@Generated("org.jsonschema2pojo")
public class InputNik {

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("nik")
    @Expose
    private String nik;
    @SerializedName("code")
    @Expose
    private String code;

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The nik
     */
    public String getNik() {
        return nik;
    }

    /**
     *
     * @param nik
     * The nik
     */
    public void setNik(String nik) {
        this.nik = nik;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }
}
