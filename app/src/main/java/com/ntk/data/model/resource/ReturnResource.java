package com.ntk.data.model.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ntk.data.model.Meta;

import javax.annotation.Generated;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
@Generated("org.jsonschema2pojo")
public class ReturnResource {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("data")
    @Expose
    private Data data;

    /**
     *
     * @return
     * The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     *
     * @param meta
     * The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

}