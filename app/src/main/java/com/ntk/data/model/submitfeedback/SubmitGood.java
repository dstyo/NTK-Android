package com.ntk.data.model.submitfeedback;

/**
 * Created by Dycode on 08/11/2016.
 */

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class SubmitGood {

    @SerializedName("brand")
    @Expose
    private Integer brand;
    @SerializedName("product")
    @Expose
    private Integer product;
    @SerializedName("isGood")
    @Expose
    private Boolean isGood;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("productionCode")
    @Expose
    private String productionCode;
    @SerializedName("conditions")
    @Expose
    private List<Conditions> conditions = new ArrayList<Conditions>();
    @SerializedName("vendor")
    @Expose
    private Vendor vendor;
    /**
     *
     * @return
     * The brand
     */
    public Integer getBrand() {
        return brand;
    }

    /**
     *
     * @param brand
     * The brand
     */
    public void setBrand(Integer brand) {
        this.brand = brand;
    }

    /**
     *
     * @return
     * The product
     */
    public Integer getProduct() {
        return product;
    }

    /**
     *
     * @param product
     * The product
     */
    public void setProduct(Integer product) {
        this.product = product;
    }

    /**
     *
     * @return
     * The isGood
     */
    public Boolean getIsGood() {
        return isGood;
    }

    /**
     *
     * @param isGood
     * The isGood
     */
    public void setIsGood(Boolean isGood) {
        this.isGood = isGood;
    }

    /**
     *
     * @return
     * The sku
     */
    public String getSku() {
        return sku;
    }

    /**
     *
     * @param sku
     * The sku
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The productionCode
     */
    public String getProductionCode() {
        return productionCode;
    }

    /**
     *
     * @param productionCode
     * The productionCode
     */
    public void setProductionCode(String productionCode) {
        this.productionCode = productionCode;
    }

    /**
     *
     * @return
     * The conditions
     */
    public List<Conditions> getConditions() {
        return conditions;
    }

    /**
     *
     * @param conditions
     * The conditions
     */
    public void setConditions(List<Conditions> conditions) {
        this.conditions = conditions;
    }

    /**
     *
     * @return
     * The vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     *
     * @param vendor
     * The vendor
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}