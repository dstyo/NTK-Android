package com.ntk.data.retrofit;

import com.ntk.constant.UrlConstant;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Dean Nurchusnul on 02/08/2016.
 */
public class AppRequestHeaderInterceptor implements Interceptor{

    @Override
    public Response intercept(Chain chain) throws IOException {
        String credential = Credentials.basic(UrlConstant.CLIENT_KEY, UrlConstant.CLIENT_PASSWORD);

        Request request = chain.request().newBuilder()
                .removeHeader("Content-Type")
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", credential)
                .build();


        return chain.proceed(request);
    }
}
