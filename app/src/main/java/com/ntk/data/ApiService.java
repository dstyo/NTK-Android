package com.ntk.data;

import com.ntk.data.model.ActionBookmark;
import com.ntk.data.model.AddComment;
import com.ntk.data.model.ChangePhoto;
import com.ntk.data.model.InputNik;
import com.ntk.data.model.Login;
import com.ntk.data.model.ReturnAddComment;
import com.ntk.data.model.ReturnBoolean;
import com.ntk.data.model.ReturnComment;
import com.ntk.data.model.ReturnListNews;
import com.ntk.data.model.ReturnLogin;
import com.ntk.data.model.ReturnProfile;
import com.ntk.data.model.ReturnString;
import com.ntk.data.model.history.ReturnHistory;
import com.ntk.data.model.news.ReturnNews;
import com.ntk.data.model.resource.ReturnResource;
import com.ntk.data.model.UpdateDeviceId;
import com.ntk.data.model.submitfeedback.SubmitGood;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public interface ApiService {
    @POST("api/v1/cekNik")
    Observable<ReturnString> postNik(@Body InputNik postNik);

    @POST("api/v1/user/reqCode")
    Observable<ReturnString> requestCode(@Body InputNik inputNik, @Query("resend") String resend);

    @POST("api/v1/user/verificationCode")
    Observable<ReturnString> verificationCode(@Body InputNik inputNik);

    @POST("api/v1/oauth2/signin")
    Observable<ReturnLogin> login(@Body Login login);

    @PUT("api/v1/user/updateToken")
    Observable<ReturnString> deviceId(@Query("access_token") String accessToken, @Body UpdateDeviceId updateDeviceId);

    @GET("api/v1/quality_feedbacks/resources")
    Observable<ReturnResource> getResource(@Query("access_token") String accessToken);

    @POST("api/v1/quality_feedbacks")
    Observable<ReturnString> submitFeedback(@Query("access_token") String accessToken, @Body SubmitGood submitGood);

    @GET("api/v1/news")
    Observable<ReturnListNews> getListNews(@Query("access_token") String accessToken);

    @GET("api/v1/user/profile")
    Observable<ReturnProfile> getProfile(@Query("access_token") String accessToken);

    @PUT("api/v1/user/avatar")
    Observable<ReturnString> changePhotoProfile(@Query("access_token") String accessToken, @Body ChangePhoto changePhoto);

    @GET("api/v1/comments")
    Observable<ReturnComment> getComment(@Query("newsId") String idNews, @Query("access_token") String accessToken);

    @POST("api/v1/comments")
    Observable<ReturnAddComment> addComment(@Query("access_token") String accessToken, @Body AddComment addComment);

    @DELETE("api/v1/comments/{commentId}")
    Observable<ReturnString> deleteComment(@Path("commentId") String commentId, @Query("access_token") String accessToken);

    @PUT("api/v1/comments/{commentId}")
    Observable<ReturnAddComment> editComment(@Path("commentId") String commentId, @Query("access_token") String accessToken, @Body AddComment addComment);

    @POST("api/v1/news/{newsId}/bookmark")
    Observable<ReturnString> Bookmark(@Path("newsId") String newsId, @Query("access_token") String accessToken, @Body ActionBookmark actionBookmark);

    @GET("api/v1/quality_feedbacks")
    Observable<ReturnHistory> getHistory(@Query("access_token") String accessToken);

    @GET("api/v1/news/active")
    Observable<ReturnNews> getNews(@Query("access_token") String accessToken);

    @GET("api/v1/news/unread")
    Observable<ReturnBoolean> getNewsUnread(@Query("access_token") String accessToken);

}
