package com.ntk.data.preference;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.ntk.QualityFeedbackApp;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class PreferenceHelper {
    private static final String PREF_LOGIN = "PREF_LOGIN";
    private static final String PREF_USER_DATA = "PREF_USER_DATA";

    private static SharedPreferences getSharedPreference() {
        return PreferenceManager.getDefaultSharedPreferences(QualityFeedbackApp.getAppContext());
    }

    public static void setLogin(boolean isLogin) {
        getSharedPreference().edit().putBoolean(PREF_LOGIN, isLogin).commit();
    }

    public static boolean isLogin() {
        return getSharedPreference().getBoolean(PREF_LOGIN, false);
    }

    public static void logout() {
        getSharedPreference().edit().clear().apply();
    }

    public static void saveUserData(String accessToken,  String username){
        UserData userData = new UserData();
        userData.setAccessToken(accessToken);
        userData.setUsername(username);
        userData.setIdNews("");
        String jsonUserData = new Gson().toJson(userData);
        getSharedPreference().edit().putString(PREF_USER_DATA, jsonUserData).commit();
    }
    public static UserData getUserData(){
        String jsonUserData = getSharedPreference().getString(PREF_USER_DATA,"");
        return new Gson().fromJson(jsonUserData, UserData.class);
    }


    public static void savenews(String id){
        UserData userData = getUserData();
        userData.setIdNews(id);
        String jsonUserData = new Gson().toJson(userData);
        getSharedPreference().edit().putString(PREF_USER_DATA, jsonUserData).commit();
    }
}
