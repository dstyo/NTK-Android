package com.ntk.data.preference;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class UserData {
    private String accessToken;
    private String username;
    private String idNews;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdNews(){
        return  idNews;
    }

    public void setIdNews(String idNews){
        this.idNews = idNews;
    }
}
