package com.ntk.data.gcm;

import com.ntk.QualityFeedbackApp;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Dycode on 24/10/2016.
 */

public class RealmGcm {
    public static RealmConfiguration mRealmConfiguration = null;

    private LocaleSave localeSave;

    Realm realm = Realm.getInstance(getRealmConfiguration());

    public static RealmConfiguration getRealmConfiguration() {
        if (mRealmConfiguration == null) {
            mRealmConfiguration = new RealmConfiguration.Builder(QualityFeedbackApp.getAppContext())
                    .schemaVersion(1)
                    .deleteRealmIfMigrationNeeded()
                    .build();
        }
        return mRealmConfiguration;
    }

    public void saveGCM(String gcm){
        if (localeSave() == null){
            int nextID = 1;
            LocaleSave localeSave = new LocaleSave();
            localeSave.setId(nextID);
            localeSave.setGcmCode(gcm);
            realm.beginTransaction();
            realm.copyToRealm(localeSave);
            realm.commitTransaction();
        }
        else {
            realm.beginTransaction();
            localeSave().setGcmCode(gcm);
            realm.commitTransaction();
        }

    }

    public LocaleSave localeSave(){
        LocaleSave localeSave = realm.where(LocaleSave.class)
                .findFirst();

        if (localeSave != null){
            return localeSave;
        }
        else {
            return null;
        }
    }
}
