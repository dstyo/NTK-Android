package com.ntk.data.gcm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dycode on 20/10/2016.
 */

public class LocaleSave extends RealmObject{

    @PrimaryKey
    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    private String gcmCode;

    public String getGcmCode() {
        return gcmCode;
    }

    public void setGcmCode(String gcmCode) {
        this.gcmCode = gcmCode;
    }
}
