package com.ntk.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by faerulsalamun on 12/15/15.
 */
public abstract class BaseAdapterItemReyclerView<Data, Holder extends BaseItemRecyclerViewHolder> extends
        RecyclerView.Adapter<Holder> {

    protected Context mContext;
    protected List<Data> mData;
    protected OnItemClickListener itemClickListener;
    protected OnLongItemClickListener longItemClickListener;

    public BaseAdapterItemReyclerView(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    protected View getView(ViewGroup parent, int viewType) {
        return LayoutInflater.from(mContext).inflate(getItemView(viewType), parent, false);
    }

    protected abstract int getItemView(int viewType);

    @Override
    public abstract Holder onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        try {
            return mData.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Data> getData() {
        return mData;
    }

    public void add(Data item) {
        mData.add(item);
        notifyItemInserted(mData.size() - 1);
    }

    public void add(Data item, int position) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void add(final List<Data> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void addAll(List<Data> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void remove(Data item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnLongItemClickListener {
        void onLongItemClick(View view, int position);
    }

    public void setOnLongItemClickListener(OnLongItemClickListener longItemClickListener) {
        this.longItemClickListener = longItemClickListener;
    }

}