package com.ntk.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.ButterKnife;

/**
 * Created by Dean Nurchusnul on 14/09/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected android.app.Activity Activity = this;
    SystemBarTintManager tintManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityView());

        ButterKnife.bind(Activity);

        tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);

        onViewReady(savedInstanceState);
    }
    protected void changeColorSystemBar(int color){
        tintManager.setStatusBarTintResource(color);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
    protected abstract int getActivityView();
    protected abstract void onViewReady(Bundle savedInstanceState);
}
