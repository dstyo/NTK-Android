package com.ntk.home.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ntk.home.fragment.HistoryFragment;
import com.ntk.home.fragment.MainFragment;
import com.ntk.home.fragment.ProfileFragment;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.27.02
 */
public class HomeTabAdapter extends FragmentPagerAdapter {

    MainFragment mainFragment;
    ProfileFragment profileFragment;

    public HomeTabAdapter(FragmentManager fm) {
        super(fm);
        mainFragment = MainFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 2) {
            return profileFragment;
        }
        else if (position == 1){
            return mainFragment;
        }
        else if (position == 0){
            return new HistoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

}