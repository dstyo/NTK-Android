package com.ntk.home.main;

import com.ntk.data.model.resource.Brand;
import com.ntk.data.model.resource.Condition;
import com.ntk.data.model.resource.Data;
import com.ntk.data.model.resource.Part;
import com.ntk.data.model.resource.Product;
import com.ntk.data.model.resource.VendorType;

import java.util.List;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public interface IMainView {
    void showLoad();
    void dissmissLoad();
    void showError(String string);
    void showData(Data data);
    void selectBrand(int selectedPos, List<Brand> listTask);
    void selectProduct(int selectedPos, List<Product> listProduct);
    void selectPart(int selectedPos, List<Part> listPart);
    void selectCondition(int selectedPos, List<Condition> listCondition);
    void selectVendor(int selectedPos, List<VendorType> listVendor);
    void successSubmit();

}
