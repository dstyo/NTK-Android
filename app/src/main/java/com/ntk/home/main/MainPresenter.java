package com.ntk.home.main;

import com.ntk.data.ApiService;
import com.ntk.data.model.ReturnString;
import com.ntk.data.model.resource.Brand;
import com.ntk.data.model.resource.Condition;
import com.ntk.data.model.resource.Data;
import com.ntk.data.model.resource.Part;
import com.ntk.data.model.resource.Product;
import com.ntk.data.model.resource.ReturnResource;
import com.ntk.data.model.resource.VendorType;
import com.ntk.data.model.submitfeedback.Conditions;
import com.ntk.data.model.submitfeedback.SubmitGood;
import com.ntk.data.model.submitfeedback.Vendor;
import com.ntk.data.preference.PreferenceHelper;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class MainPresenter {
    IMainView iMainView;
    ApiService apiService;
    Data datas;

    public MainPresenter(IMainView iMainView, ApiService apiService){
        this.iMainView = iMainView;
        this.apiService = apiService;
    }

    public void getResource(){
        iMainView.showLoad();
        apiService.getResource(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnResource>>) new Func1<Throwable, Observable<? extends ReturnResource>>() {
                    @Override
                    public Observable<? extends ReturnResource> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnResource>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iMainView.showError(e.getMessage());
                        iMainView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnResource resource) {
                        if(resource.getData().getBrands().isEmpty()){
                            iMainView.showError(resource.getMeta().getMessage());
                            iMainView.dissmissLoad();
                        }else{
                            iMainView.showData(resource.getData());
                            datas = resource.getData();
                            iMainView.dissmissLoad();
                        }


                    }
                });
    }
    public void selectBrand(int position, List<Brand> data) {
        for (int pos = 0; pos < data.size(); pos++) {
            data.get(pos).setSelected(false);
        }
        data.get(position).setSelected(true);
        iMainView.selectBrand(position, data);
    }
    public void selectProduct(int position, List<Product> data) {
        for (int pos = 0; pos < data.size(); pos++) {
            data.get(pos).setSelected(false);
        }
        data.get(position).setSelected(true);
        iMainView.selectProduct(position, data);
    }
    public void selectPart(int position, List<Part> data) {
        for (int pos = 0; pos < data.size(); pos++) {
            data.get(pos).setSelected(false);
        }
        data.get(position).setSelected(true);
        iMainView.selectPart(position, data);
    }
    public void selectVendor(int position, List<VendorType> data) {
        for (int pos = 0; pos < data.size(); pos++) {
            data.get(pos).setSelected(false);
        }
        data.get(position).setSelected(true);
        iMainView.selectVendor(position, data);
    }
    public void selectCondition(int position, List<Condition> data) {
        for (int pos = 0; pos < datas.getConditions().size(); pos++) {
            datas.getConditions().get(pos).setSelected(false);
        }
        data.get(position).setSelected(true);
        iMainView.selectCondition(position, data);
    }

    public void submitFeedback(int idBrand, int idProduct, Boolean isGood, String sku, String photo, String desc, String prodCode, List<Conditions> conditionsList, Vendor vendor){
        iMainView.showLoad();
        SubmitGood submitGood = new SubmitGood();
        submitGood.setBrand(idBrand);
        submitGood.setProduct(idProduct);
        submitGood.setIsGood(isGood);
        submitGood.setSku(sku);
        submitGood.setPhoto(photo);
        if (isGood){
            submitGood.setDescription(desc);
        }
        else {
            //submitGood.setProductionCode(prodCode);
            submitGood.setProductionCode("");
            submitGood.setConditions(conditionsList);
            submitGood.setVendor(vendor);
        }

        apiService.submitFeedback(PreferenceHelper.getUserData().getAccessToken(), submitGood)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iMainView.showError(e.getMessage());
                        iMainView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getMeta().getCode()==200) {
                            iMainView.successSubmit();
                            iMainView.dissmissLoad();
                        }
                        else {
                            iMainView.showError(returnString.getData());
                            iMainView.dissmissLoad();
                        }
                    }
                });
    }
}
