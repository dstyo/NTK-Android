package com.ntk.home;

import com.ntk.data.ApiService;
import com.ntk.data.model.ReturnBoolean;
import com.ntk.data.model.news.ReturnNews;
import com.ntk.data.preference.PreferenceHelper;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dycode on 28/10/2016.
 */

public class HomePresenter {
    IHomeView iHomeView;
    ApiService apiService;

    public HomePresenter(IHomeView iHomeView, ApiService apiService){
        this.iHomeView = iHomeView;
        this.apiService = apiService;
    }

    public void checkDialog(final String idNews){
        iHomeView.showLoad();
        apiService.getNews(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnNews>>) new Func1<Throwable, Observable<? extends ReturnNews>>() {
                    @Override
                    public Observable<? extends ReturnNews> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnNews>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iHomeView.showError(e.getMessage());
                        iHomeView.dissmissLoad();
                    }
                    @Override
                    public void onNext(ReturnNews resource) {
                        if (!resource.getData().equals(null) && !resource.getData().getId().equals(PreferenceHelper.getUserData().getIdNews())){
                            PreferenceHelper.savenews(resource.getData().getId());
                            iHomeView.showDialog(resource.getData().getId());
                        }
                        else if (idNews != ""){
                            iHomeView.showDialog(idNews);
                        }
                    }
                });
    }

    public void checkNewsUnread(){
        iHomeView.showLoad();
        apiService.getNewsUnread(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnBoolean>>) new Func1<Throwable, Observable<? extends ReturnBoolean>>() {
                    @Override
                    public Observable<? extends ReturnBoolean> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnBoolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iHomeView.showError(e.getMessage());
                        iHomeView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnBoolean resource) {
                        iHomeView.dissmissLoad();
                        iHomeView.unRead(resource.getData());
                    }
                });
    }

}
