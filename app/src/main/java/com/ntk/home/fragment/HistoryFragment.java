package com.ntk.home.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.data.ApiService;
import com.ntk.data.model.history.Datum;
import com.ntk.home.history.HistoryPresenter;
import com.ntk.home.history.HistoryView;
import com.ntk.home.resource.history.HistoryBadRCAdapter;
import com.ntk.utils.CustomRecycleView;
import com.ntk.utils.LayoutUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.27.02
 */
public class HistoryFragment extends Fragment implements HistoryView {

    ProgressDialog progressDialog;
    @BindView(R.id.text_null)
    TextView textNull;
    @BindView(R.id.layout_history)
    LinearLayout layoutHistory;
    LinearLayout layoutDetailHistory;
    @Inject
    ApiService apiService;
    HistoryPresenter historyPresenter;
    HistoryBadRCAdapter historyBadRCAdapter;

    List<Datum> dataHistory;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.content_history, container, false);
        ButterKnife.bind(this, v);
        ((QualityFeedbackApp) getActivity().getApplication())
                .getComponent()
                .inject(this);
        historyPresenter = new HistoryPresenter(apiService, this);
        historyPresenter.getDataHistory();
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                historyPresenter.getDataHistory();
            }
        });
        // Configure the refreshing colors
        swiperefresh.setColorSchemeResources(R.color.nutricia_purple);

        return v;

    }

    @Override
    public void showLoad() {
        swiperefresh.setRefreshing(true);
    }

    @Override
    public void dissmissLoad() {
        swiperefresh.setRefreshing(false);

    }

    @Override
    public void showError(String string) {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(string)
                .show();
    }

    @Override
    public void showDataNews(List<Datum> data) {
        layoutHistory.removeAllViews();
        dataHistory = data;
        textNull.setVisibility(View.GONE);
        for (int i = 0; i < data.size(); i++) {
            View history;
            history = getActivity().getLayoutInflater().inflate(R.layout.item_history, null);
            TextView textDate = ButterKnife.findById(history, R.id.text_date);
            CustomRecycleView rvBadHistory = ButterKnife.findById(history, R.id.rv_history);
            layoutDetailHistory = ButterKnife.findById(history, R.id.layout_detail);

            listHistory(i);
//
//            historyBadRCAdapter = new HistoryBadRCAdapter(getActivity(), getResources());
//
//            historyBadRCAdapter.clear();
//            for (int hd = 0; hd < data.get(i).getHistory().size(); hd++){
//                historyBadRCAdapter.add(data.get(i).getHistory().get(hd));
//            }
//
//            rvBadHistory.setUpAsList();
//            rvBadHistory.setAdapter(historyBadRCAdapter);

            textDate.setText(data.get(i).getDate_());

//            historyBadRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    historyPresenter.selectHistory(historyBadRCAdapter.getData().get(position).getId());
//                }
//            });

            layoutHistory.addView(history);

        }
        swiperefresh.setRefreshing(false);

    }

    private void listHistory(final int id) {
        layoutDetailHistory.removeAllViews();
        for (int i = 0; i < dataHistory.get(id).getHistory().size(); i++) {
            View viewDetail;
            viewDetail = getActivity().getLayoutInflater().inflate(R.layout.item_history_detail, null);

            ImageView imgBad = ButterKnife.findById(viewDetail, R.id.img_bad);
            TextView textBad = ButterKnife.findById(viewDetail, R.id.text_bad);
            TextView textDate = ButterKnife.findById(viewDetail, R.id.text_date);
            ImageView imgNews = ButterKnife.findById(viewDetail, R.id.img_news);
            TextView textAddress = ButterKnife.findById(viewDetail, R.id.text_address);
            TextView textStore = ButterKnife.findById(viewDetail, R.id.text_store);
            View viewGood = ButterKnife.findById(viewDetail, R.id.view_good);
            View viewBad = ButterKnife.findById(viewDetail, R.id.view_bad);
            TextView textSku = ButterKnife.findById(viewDetail, R.id.text_sku);
            TextView textPart = ButterKnife.findById(viewDetail, R.id.text_part);
            TextView textCondition = ButterKnife.findById(viewDetail, R.id.text_condition);
            LinearLayout layoutDetail = ButterKnife.findById(viewDetail, R.id.layout_detail);
            if (!dataHistory.get(id).getHistory().get(i).getPhoto().equals("")) {
                Glide.with(this).load(dataHistory.get(id).getHistory().get(i).getPhoto()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgNews);
            }

            textSku.setText(dataHistory.get(id).getHistory().get(i).getBrand() + " - " + dataHistory.get(id).getHistory().get(i).getSku());
            textDate.setText(dataHistory.get(id).getHistory().get(i).getTimePost());

            if (dataHistory.get(id).getHistory().get(i).getIsGood()) {
                textBad.setText(getResources().getString(R.string.addReport_title_type_good));
                imgBad.setImageResource(R.drawable.btn_beritabaikaktif);
                textAddress.setVisibility(View.GONE);
                textStore.setVisibility(View.GONE);
                textCondition.setVisibility(View.GONE);
                textPart.setText(dataHistory.get(id).getHistory().get(i).getDescription());
                viewBad.setVisibility(View.GONE);
                viewGood.setVisibility(View.VISIBLE);
            } else {
                textBad.setText(getResources().getString(R.string.addReport_title_type_bad));
                imgBad.setImageResource(R.drawable.btn_beritaburukaktif);
                textAddress.setVisibility(View.VISIBLE);
                textStore.setVisibility(View.VISIBLE);
                textAddress.setText(dataHistory.get(id).getHistory().get(i).getVendor().getVendorAddress());
                textStore.setText(dataHistory.get(id).getHistory().get(i).getVendor().getVendorName());
                textCondition.setVisibility(View.VISIBLE);
                textPart.setText(dataHistory.get(id).getHistory().get(i).getConditions().get(0).getPart());
                textCondition.setText(dataHistory.get(id).getHistory().get(i).getConditions().get(0).getDescription());
                viewBad.setVisibility(View.VISIBLE);
                viewGood.setVisibility(View.GONE);
            }

            if (dataHistory.get(id).getHistory().get(i).getSelected()) {
                LayoutUtils.showExpandable(layoutDetail);

            } else {
                LayoutUtils.hideExpandable(layoutDetail);
            }
            final int finalI = i;
            viewDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    historyPresenter.selectHistory(dataHistory.get(id).getHistory().get(finalI).getId());
//                    showDataNews(dataHistory);
                }
            });
            layoutDetailHistory.addView(viewDetail);
        }
    }

    @Override
    public void showNullData() {
        layoutHistory.setVisibility(View.GONE);
        textNull.setVisibility(View.VISIBLE);
    }

    @Override
    public void selected() {
        historyBadRCAdapter.notifyDataSetChanged();
    }
}
