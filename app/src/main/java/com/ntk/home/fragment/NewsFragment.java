package com.ntk.home.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ntk.NewsDetail.NewsDetailActivity;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.ApiService;
import com.ntk.data.model.ListNews;
import com.ntk.home.news.NewsPresenter;
import com.ntk.home.news.NewsView;
import com.ntk.home.resource.news.NewsRCAdapter;
import com.ntk.utils.CustomRecycleView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dean Nurchusnul on 14/09/2016.
 */
public class NewsFragment extends Fragment implements NewsView {

    private static final String ARG_POSITION = "position";
    @BindView(R.id.input_search)
    EditText inputSearch;
    @BindView(R.id.linearLogin)
    CardView linearLogin;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_bookmark)
    ImageView imgBookmark;
    @BindView(R.id.layout_search)
    LinearLayout layoutSearch;
    @BindView(R.id.rv_news)
    CustomRecycleView rvNews;

    ProgressDialog progressDialog;
    NewsPresenter homePresenter;
    NewsRCAdapter newsRCAdapter;

    @Inject
    ApiService apiService;
    @BindView(R.id.text_null)
    TextView textNull;
    List<ListNews> newses = new ArrayList<>();
    @BindView(R.id.text_null_search)
    TextView textNullSearch;
    MaterialDialog materialDialog;
    String search = "";
    Boolean bookmark = false;
    @BindView(R.id.web_news)
    WebView webNews;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    public static NewsFragment newInstance() {
        NewsFragment f = new NewsFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.content_news, container, false);
        ((QualityFeedbackApp) getActivity().getApplication())
                .getComponent()
                .inject(this);
        ButterKnife.bind(this, rootView);
        homePresenter = new NewsPresenter(apiService, this);
        newsRCAdapter = new NewsRCAdapter(getActivity(), this);
        homePresenter.getListNews();
        ButterKnife.bind(this, rootView);

        newsRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                homePresenter.getListNews();
                Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
                intent.putExtra("idNews", newsRCAdapter.getData().get(position).getId());
                intent.putExtra("bookmark", newsRCAdapter.getData().get(position).getBookmarked());
                startActivityForResult(intent, 2);
            }
        });
//        webNews.getSettings().setJavaScriptEnabled(true);
//        webNews.getSettings().setLoadWithOverviewMode(true);
//        webNews.getSettings().setUseWideViewPort(true);
//        webNews.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//
//            @Override
//            public void onPageFinished(WebView view, final String url) {
//            }
//        });
//
//        if (PreferenceHelper.getUserData().getIdNews() != 0){
//            textNull.setVisibility(View.GONE);
//            webNews.loadUrl(UrlConstant.ROOT + "api/v1/news/" + PreferenceHelper.getUserData().getIdNews() + "?access_token=" + PreferenceHelper.getUserData().getAccessToken());
//
//        }
//        else {
//            webNews.setVisibility(View.GONE);
//            textNull.setVisibility(View.VISIBLE);
//        }
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homePresenter.getListNews();
            }
        });
        // Configure the refreshing colors
        swiperefresh.setColorSchemeResources(R.color.deepskyblue);
        return rootView;
    }

    @Override
    public void showLoad() {
        swiperefresh.setRefreshing(true);
    }

    @Override
    public void dissmissLoad() {
        swiperefresh.setRefreshing(false);

    }
    @Override
    public void showError(String string) {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(string)
                .show();
    }

    @Override
    public void showDataNews(final List<ListNews> listNewses) {
        newsRCAdapter.clear();
        newsRCAdapter.add(listNewses);
        newses = listNewses;
        newsRCAdapter.notifyDataSetChanged();
        rvNews.setVisibility(View.VISIBLE);
        textNull.setVisibility(View.GONE);
        rvNews.setAdapter(newsRCAdapter);
        rvNews.setUpAsList();
        bookmark = false;
        imgBookmark.setImageResource(R.mipmap.favorite_grey_off);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                homePresenter.filterNews(inputSearch.getText().toString(), bookmark);
                search = inputSearch.getText().toString();
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                homePresenter.filterNews(inputSearch.getText().toString(), bookmark);
                search = inputSearch.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                homePresenter.filterNews(inputSearch.getText().toString(), bookmark);
                search = inputSearch.getText().toString();
            }
        });
    }

    @Override
    public void showNullNews() {
        textNull.setVisibility(View.VISIBLE);
        rvNews.setVisibility(View.GONE);
    }

    @Override
    public void showDataFilter(List<ListNews> listNewses) {
        newsRCAdapter.clear();
        if (listNewses.size() != 0) {
            newsRCAdapter.add(listNewses);
            textNullSearch.setVisibility(View.GONE);
            rvNews.setVisibility(View.VISIBLE);
        } else {
            textNullSearch.setVisibility(View.VISIBLE);
            rvNews.setVisibility(View.GONE);
        }
        newsRCAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAddBookmark(final String idNews, String title, Boolean add) {
//        mainPresenter.addBookmark(idNews);
        if (add) {
            materialDialog = new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.news_bookmark_title_delete))
                    .content(getString(R.string.news_bookmark_content_delete, title))
                    .positiveText(R.string.dialog_yes)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            homePresenter.addBookmark(idNews);
                        }
                    })
                    .negativeText(R.string.dialog_no)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            materialDialog.dismiss();
                        }
                    })
                    .show();
        } else {
            materialDialog = new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.news_bookmark_title))
                    .content(getString(R.string.news_bookmark_content, title))
                    .positiveText(R.string.dialog_yes)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            homePresenter.addBookmark(idNews);
                        }
                    })
                    .negativeText(R.string.dialog_no)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            materialDialog.dismiss();
                        }
                    })
                    .show();
        }

    }

    @Override
    public void addBookmark() {
        newsRCAdapter.notifyDataSetChanged();
    }

    public void refresh(){
        homePresenter.getListNews();
    }
    @OnClick(R.id.img_bookmark)
    public void onClick() {
        if (bookmark) {
            imgBookmark.setImageResource(R.mipmap.favorite_grey_off);
            bookmark = false;
        } else {
            imgBookmark.setImageResource(R.mipmap.favorite_grey_on);
            bookmark = true;
        }
        homePresenter.bookmark(bookmark);
    }
}
