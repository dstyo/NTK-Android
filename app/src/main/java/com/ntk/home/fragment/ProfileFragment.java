package com.ntk.home.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.SplashScreen.SplashScreenActivity;
import com.ntk.data.ApiService;
import com.ntk.data.model.Profile;
import com.ntk.data.preference.PreferenceHelper;
import com.ntk.home.profile.ProfilePresenter;
import com.ntk.home.profile.ProfileView;
import com.ntk.setting.SettingActivity;
import com.ntk.utils.CommonUtils;
import com.ntk.utils.LayoutUtils;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Dean Nurchusnul on 14/09/2016.
 */
public class ProfileFragment extends Fragment implements ProfileView {
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.text_email)
    TextView textEmail;
    @BindView(R.id.text_phone)
    TextView textPhone;
    @BindView(R.id.text_nik)
    TextView textNik;
    @BindView(R.id.imagePhoto)
    ImageView imagePhoto;
    @BindView(R.id.text_divisi)
    TextView textDivisi;
    @BindView(R.id.text_location)
    TextView textLocation;
    @BindView(R.id.img_more)
    ImageView imgMore;

    ProgressDialog progressDialog;
    @Inject
    ApiService apiService;

    ProfilePresenter profilePresenter;
    Boolean more = false;
    @BindView(R.id.layout_refresh)
    LinearLayout layoutRefresh;
    @BindView(R.id.layout_setting)
    LinearLayout layoutSetting;
    @BindView(R.id.layout_logout)
    LinearLayout layoutLogout;
    @BindView(R.id.layoutmore)
    LinearLayout layoutMore;
    @BindView(R.id.layout_more)
    RelativeLayout layout_More;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_profile, container, false);
        ButterKnife.bind(this, view);
        ((QualityFeedbackApp) getActivity().getApplication())
                .getComponent()
                .inject(this);

        profilePresenter = new ProfilePresenter(this, apiService);

        profilePresenter.getProfile();

        //layoutRefresh.setVisibility(View.GONE);

        return view;
    }

    public static ProfileFragment newInstance(){
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @OnClick(R.id.img_more)
    public void onMore() {
        if (more) {
            LayoutUtils.hideExpandable(layoutMore);
            layout_More.setVisibility(View.GONE);
            more = false;
        } else {
            LayoutUtils.showExpandable(layoutMore);
            layout_More.setVisibility(View.VISIBLE);
            more = true;
        }
    }

    @Override
    public void showLoad() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.dialog_progress));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void dissmissLoad() {
        progressDialog.dismiss();
    }

    @Override
    public void showError(String string) {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(string)
                .show();

        if (string.equals("HTTP 401 Unauthorized")) {
            PreferenceHelper.setLogin(false);
            PreferenceHelper.logout();
            Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void showProfile(Profile profile) {
        textName.setText(profile.getFullname());
        textEmail.setText(profile.getEmail());
        textPhone.setText(profile.getPhone());
        textNik.setText(profile.getNik());
        textDivisi.setText(profile.getDivision());
        textLocation.setText(profile.getLocation());

        if (!profile.getPicture().equals("")) {
            Glide.with(this).load(profile.getPicture()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imagePhoto);
        }
    }

    @OnClick(R.id.layout_more)
    public void onHiddenMore() {
        LayoutUtils.hideExpandable(layoutMore);
        layout_More.setVisibility(View.GONE);
        more = false;
    }

    @OnClick({R.id.layout_setting, R.id.layout_logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_setting:
                Intent i = new Intent(getActivity(), SettingActivity.class);
                onHiddenMore();
                startActivity(i);
                break;
            case R.id.layout_logout:
                PreferenceHelper.setLogin(false);
                PreferenceHelper.logout();
                Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }

    @OnClick(R.id.imagePhoto)
    public void onClick() {
        camera();
    }


    public void camera() {
        final String arr[] = {"Gallery", "Camera"};
        new MaterialDialog.Builder(getActivity())
                .title("Silahkan Pilih")
                .items(arr)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int pos, CharSequence text) {

                        if (pos == 1) {
                            RxPaparazzo.takeImage(getActivity())
                                    .usingCamera()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(response -> {
                                        // See response.resultCode() doc
                                        if (response.resultCode() != RESULT_OK) {
                                            return;
                                        }
                                        try
                                        {
                                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver() , Uri.parse("file://" + response.data()));
                                            afterPhoto(bitmap);
                                        }
                                        catch (Exception e)
                                        {
                                            CommonUtils.showToast(getActivity(), e.getMessage());
                                        }
                                    });
                        } else {
                            RxImagePicker.with(getActivity()).requestImage(Sources.GALLERY)
                                    .flatMap(new Func1<Uri, Observable<Bitmap>>() {
                                        @Override
                                        public Observable<Bitmap> call(Uri uri) {
                                            return RxImageConverters.uriToBitmap(getActivity(), uri);
                                        }
                                    })
                                    .subscribe(new Action1<Bitmap>() {
                                        @Override
                                        public void call(Bitmap bitmap) {
                                            afterPhoto(bitmap);
                                        }
                                    });
                        }

                        dialog.dismiss();
                        return true;
                    }
                })
                .alwaysCallInputCallback()
                .show();
    }

    public void afterPhoto(Bitmap photo) {
        profilePresenter.changeProfile(encodeToBase64(photo));
        imagePhoto.setImageBitmap(photo);
    }
    public static String encodeToBase64(Bitmap image) {
        if (image!=null){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            return "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        else {
            return "";
        }
    }
}
