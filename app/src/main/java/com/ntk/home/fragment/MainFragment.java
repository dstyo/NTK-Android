package com.ntk.home.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.SplashScreen.SplashScreenActivity;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.ApiService;
import com.ntk.data.model.resource.Brand;
import com.ntk.data.model.resource.Condition;
import com.ntk.data.model.resource.Data;
import com.ntk.data.model.resource.Part;
import com.ntk.data.model.resource.Product;
import com.ntk.data.model.resource.VendorType;
import com.ntk.data.model.submitfeedback.Conditions;
import com.ntk.data.model.submitfeedback.Vendor;
import com.ntk.data.preference.PreferenceHelper;
import com.ntk.home.HomeActivity;
import com.ntk.home.main.IMainView;
import com.ntk.home.main.MainPresenter;
import com.ntk.home.resource.brand.BrandRCAdapter;
import com.ntk.home.resource.part.PartRCAdapter;
import com.ntk.home.resource.partcondition.PartConditionRCAdapter;
import com.ntk.home.resource.product.ProductRCAdapter;
import com.ntk.home.resource.vendor.VendorRCAdapter;
import com.ntk.utils.CommonUtils;
import com.ntk.utils.CustomRecycleView;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.28.02
 */
public class MainFragment extends Fragment implements IMainView {
    @BindView(R.id.imgBackground)
    ImageView imgBackground;
    @BindView(R.id.imagePhoto)
    ImageView imagePhoto;
    ProgressDialog progressDialog;
    MainPresenter mainPresenter;
    @Inject
    ApiService apiService;
    @BindView(R.id.img_report_good)
    ImageView imgReportGood;
    @BindView(R.id.img_report_bad)
    ImageView imgReportBad;
    @BindView(R.id.text_list_product)
    TextView textListProduct;
    @BindView(R.id.layout_product)
    LinearLayout layoutProduct;
    @BindView(R.id.layout_add_photo)
    LinearLayout layoutAddPhoto;
    @BindView(R.id.layout_input_image)
    LinearLayout layoutInputImage;
    /*@BindView(R.id.input_code_1)
    EditText inputCode1;
    @BindView(R.id.input_code_2)
    EditText inputCode2;
    @BindView(R.id.input_code_3)
    EditText inputCode3;
    @BindView(R.id.input_code_4)
    EditText inputCode4;
    @BindView(R.id.input_code_5)
    EditText inputCode5;
    @BindView(R.id.input_code_6)
    EditText inputCode6;
    @BindView(R.id.input_time_1)
    EditText inputTime1;
    @BindView(R.id.input_time_2)
    EditText inputTime2;
    @BindView(R.id.spinnerKodeProduksi)
    MaterialSpinner spinnerKodeProduksi;
    @BindView(R.id.input_code_7)
    EditText inputCode7;
    @BindView(R.id.input_code_8)
    EditText inputCode8;
    @BindView(R.id.input_code_9)
    EditText inputCode9;
    @BindView(R.id.input_code_10)
    EditText inputCode10;*/
    @BindView(R.id.img_product)
    ImageView imgProduct;
    @BindView(R.id.layout_item_task)
    LinearLayout layoutItemTask;
    @BindView(R.id.input_address_store)
    EditText inputAddressStore;
    @BindView(R.id.spin_city)
    MaterialSpinner spinCity;
    @BindView(R.id.spin_prov)
    MaterialSpinner spinProv;
    @BindView(R.id.layout_bad_report)
    LinearLayout layoutBadReport;
    @BindView(R.id.edit_good_news)
    EditText editGoodNews;
    @BindView(R.id.layout_good_report)
    LinearLayout layoutGoodReport;
    @BindView(R.id.text_button_login)
    TextView textButtonLogin;
    @BindView(R.id.button_login)
    MaterialRippleLayout buttonLogin;
    @BindView(R.id.cardView)
    CardView cardView;
    @BindView(R.id.text_report_good)
    TextView textReportGood;
    @BindView(R.id.text_report_bad)
    TextView textReportBad;
    @BindView(R.id.layout_vendor)
    LinearLayout layoutVendor;
    @BindView(R.id.rv_content)
    CustomRecycleView rvBrand;
    @BindView(R.id.rv_vendor)
    CustomRecycleView rvVendor;
    @BindView(R.id.autocompleteView)
    AutoCompleteTextView textStore;

    Data dataResource;

    StringBuilder sb = new StringBuilder();
    @BindView(R.id.rv_part)
    CustomRecycleView rvPart;
    BrandRCAdapter brandRCAdapter;
    ProductRCAdapter productRCAdapter;
    PartRCAdapter partRCAdapter;
    VendorRCAdapter vendorRCAdapter;
    PartConditionRCAdapter partConditionRCAdapter;

    ArrayList<String> kodeProd = new ArrayList<>();
    ArrayList<String> modernTrade = new ArrayList<>();
    List<String> sCity = new ArrayList<String>();
    List<String> sProvince = new ArrayList<String>();
    CustomRecycleView rvCondition;
    Dialog dialog, dialogCondition, dialogSummary;
    ArrayAdapter adapterTrade;
    String idPart, textMore = "";
    //String prodCode;
    Condition partCondition;
    TextView text_done;
    LocationManager locManager;
    LocationListener locListener = new MyLocationListener();
    boolean gps_enabled = false, report;
    boolean network_enabled = false;
    Brand brands = new Brand();
    Product product = new Product();
    Part part = new Part();
    ArrayAdapter<String> adapterKodeProduksi;

    VendorType vendort = new VendorType();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_home, container, false);
        ButterKnife.bind(this, view);
        ((QualityFeedbackApp) getActivity().getApplication())
                .getComponent()
                .inject(this);

        brandRCAdapter = new BrandRCAdapter(getActivity(), this);
        productRCAdapter = new ProductRCAdapter(getActivity(), this);
        partRCAdapter = new PartRCAdapter(getActivity(), this);
        vendorRCAdapter = new VendorRCAdapter(getActivity(), this);
        partConditionRCAdapter = new PartConditionRCAdapter(getActivity(), this);

        rvBrand.setAdapter(brandRCAdapter);
        rvVendor.setAdapter(vendorRCAdapter);
        rvVendor.setUpAsList();
        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        brandRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainPresenter.selectBrand(position, brandRCAdapter.getData());
                brands = brandRCAdapter.getData().get(position);
                cekInput();
            }
        });
        productRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainPresenter.selectProduct(position, productRCAdapter.getData());
                product = productRCAdapter.getData().get(position);
                cekInput();
            }
        });
        partRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                partConditionRCAdapter.clear();
                for (int i = 0; i < dataResource.getConditions().size(); i++) {
                    if (dataResource.getConditions().get(i).getPart().equals(partRCAdapter.getData().get(position).getId())) {
                        partConditionRCAdapter.add(dataResource.getConditions().get(i));
                    }
                }
                idPart = partRCAdapter.getData().get(position).getId();
                dialogCondition(partRCAdapter.getData().get(position).getName(), position);
                cekInput();

            }
        });
        vendorRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainPresenter.selectVendor(position, vendorRCAdapter.getData());
                vendort = vendorRCAdapter.getData().get(position);
                cekInput();
            }
        });

        partConditionRCAdapter.setOnItemClickListener(new BaseAdapterItemReyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mainPresenter.selectCondition(position, partConditionRCAdapter.getData());
                partCondition = partConditionRCAdapter.getData().get(position);
            }
        });

        adapterTrade = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, modernTrade);

        getloc();
        if (sCity.size() == 0 && sProvince.size() == 0) {
            sCity.add("Jakarta");
            sProvince.add("DKI Jakarta");
        }
        setupPresenter();
        mainPresenter.getResource();
        disable();
        edittext();
        return view;
    }

    public static MainFragment newInstance() {
        MainFragment mainFragments = new MainFragment();
        return mainFragments;
    }

    @Override
    public void showLoad() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.dialog_progress));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void dissmissLoad() {
        progressDialog.dismiss();
    }

    @Override
    public void showError(String string) {
        new MaterialDialog.Builder(getActivity())
                .title(getString(R.string.dialog_error_title))
                .content(string)
                .show();

        if (string.equals("HTTP 401 Unauthorized")) {
            PreferenceHelper.setLogin(false);
            PreferenceHelper.logout();
            Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void showData(Data data) {
        ArrayList<String> city = new ArrayList<>();


        brandRCAdapter.clear();
        for (int i = 0; i < data.getBrands().size(); i++) {
            if (data.getBrands().get(i).getType().equals("all")) {
                brandRCAdapter.add(data.getBrands().get(i));
            }
        }
        for (int i = 0; i < data.getTrades().size(); i++) {
            city.add(data.getTrades().get(i).getName());
        }
        vendorRCAdapter.clear();
        vendorRCAdapter.add(data.getVendorTypes());
        rvBrand.setUpAsGrid(brandRCAdapter.getItemCount());
//        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, city);
//        autoComplete.setAdapter(itemsAdapter);
        dataResource = data;
    }

    private void setupPresenter() {
        if (mainPresenter == null) {
            mainPresenter = new MainPresenter(this, apiService);
        }
    }

    @OnClick({R.id.img_report_good, R.id.img_report_bad})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_report_good:
                imgReportGood.setImageResource(R.drawable.btn_beritabaikaktif);
                imgReportBad.setImageResource(R.drawable.btn_beritaburuknaktif);
                layoutBadReport.setVisibility(View.GONE);
                textReportGood.setTextColor(getResources().getColor(R.color.color_light_green_quality_survey));
                textReportBad.setTextColor(getResources().getColor(R.color.text_grey_quality_survey));
                layoutGoodReport.setVisibility(View.VISIBLE);
                report = true;
                cekInput();
                break;
            case R.id.img_report_bad:
                imgReportBad.setImageResource(R.drawable.btn_beritaburukaktif);
                imgReportGood.setImageResource(R.drawable.btn_beritaburuknaktif);
                layoutGoodReport.setVisibility(View.GONE);
                textReportBad.setTextColor(getResources().getColor(R.color.color_light_red_quality_survey));
                textReportGood.setTextColor(getResources().getColor(R.color.text_grey_quality_survey));
                layoutBadReport.setVisibility(View.VISIBLE);
                report = false;
                cekInput();
                break;
        }
    }

    public void camera() {
        final String arr[] = {"Gallery", "Camera"};
        new MaterialDialog.Builder(getActivity())
                .title("Silahkan Pilih")
                .items(arr)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int pos, CharSequence text) {

                        if (pos == 1) {
                            RxPaparazzo.takeImage(getActivity())
                                    .usingCamera()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(response -> {
                                        // See response.resultCode() doc
                                        if (response.resultCode() != RESULT_OK) {
                                            return;
                                        }
                                        try
                                        {
                                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver() , Uri.parse("file://" + response.data()));
                                            afterPhoto(bitmap);
                                        }
                                        catch (Exception e)
                                        {
                                            CommonUtils.showToast(getActivity(), e.getMessage());
                                        }
                                    });
                        } else {
                            RxImagePicker.with(getActivity()).requestImage(Sources.GALLERY)
                                    .flatMap(new Func1<Uri, Observable<Bitmap>>() {
                                        @Override
                                        public Observable<Bitmap> call(Uri uri) {
                                            return RxImageConverters.uriToBitmap(getActivity(), uri);
                                        }
                                    })
                                    .subscribe(new Action1<Bitmap>() {
                                        @Override
                                        public void call(Bitmap bitmap) {
                                            afterPhoto(bitmap);
                                        }
                                    });
                        }

                        dialog.dismiss();
                        return true;
                    }
                })
                .alwaysCallInputCallback()
                .show();
    }

    public void afterPhoto(Bitmap photo) {

        imagePhoto.setImageBitmap(photo);
        Bitmap blurred = CommonUtils.blurRenderScript(getActivity(), photo, 25);
        imgBackground.setImageBitmap(blurred);
        layoutAddPhoto.setVisibility(View.GONE);
        cekInput();
    }

    public static String encodeToBase64(Bitmap image) {
        if (image != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return "data:image/png;base64," + Base64.encodeToString(byteArray, Base64.DEFAULT);
        } else {
            return "";
        }
    }

    @OnClick(R.id.layout_input_image)
    public void getImage() {
        camera();
    }

    public void edittext() {
        /*inputCode1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (sb.length() == 0 & inputCode1.length() == 1) {
                    sb.append(s);
                    inputCode1.clearFocus();
                    inputCode2.requestFocus();
                    inputCode2.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode1.length() < 1) {
                    inputCode1.requestFocus();
                }
            }
        });
        inputCode2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (sb.length() == 0 & inputCode2.length() == 1) {
                    sb.append(s);
                    inputCode2.clearFocus();
                    inputCode3.requestFocus();
                    inputCode3.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode2.length() < 1) {
                    inputCode2.requestFocus();
                }
            }
        });
        inputCode3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode3.length() == 1) {
                    sb.append(s);
                    inputCode3.clearFocus();
                    inputCode4.requestFocus();
                    inputCode4.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode3.length() < 1) {
                    inputCode3.requestFocus();
                }
            }
        });
        inputCode4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (sb.length() == 0) {
                    sb.append(s);
                    inputCode4.clearFocus();
                    inputCode5.requestFocus();
                    inputCode5.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode4.length() < 1) {
                    inputCode4.requestFocus();
                }
            }
        });
        inputCode5.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode5.length() == 1) {
                    sb.append(s);
                    inputCode5.clearFocus();
                    inputCode6.requestFocus();
                    inputCode6.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode5.length() < 1) {
                    inputCode5.requestFocus();
                }
            }
        });
        inputCode6.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode6.length() == 1) {
                    sb.append(s);
                    inputCode6.clearFocus();
                    inputTime1.requestFocus();
                    inputTime1.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode6.length() < 1) {
                    inputCode6.requestFocus();
                }
            }
        });
        inputTime1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputTime1.length() == 2) {
                    sb.append(s);
                    inputTime1.clearFocus();
                    inputTime2.requestFocus();
                    inputTime2.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputTime1.length() < 2) {
                    inputTime1.requestFocus();
                }
            }
        });
        inputTime2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputTime2.length() == 2) {
                    sb.append(s);
                    inputTime2.clearFocus();
                    inputCode7.requestFocus();
                    inputCode7.setCursorVisible(true);
                    cekInput();

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputTime2.length() < 2) {
                    inputTime2.requestFocus();
                }
            }
        });
        inputCode7.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode7.length() == 1) {
                    sb.append(s);
                    inputCode7.clearFocus();
                    inputCode8.requestFocus();
                    inputCode8.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode7.length() < 1) {
                    inputCode7.requestFocus();
                }
            }
        });
        inputCode8.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode8.length() == 1) {
                    sb.append(s);
                    inputCode8.clearFocus();
                    inputCode9.requestFocus();
                    inputCode9.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode8.length() < 1) {
                    inputCode8.requestFocus();
                }
            }
        });
        inputCode9.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode9.length() == 1) {
                    sb.append(s);
                    inputCode9.clearFocus();
                    inputCode10.requestFocus();
                    inputCode10.setCursorVisible(true);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode9.length() < 1) {
                    inputCode9.requestFocus();
                }
            }
        });
        inputCode10.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputCode10.length() == 1) {
                    sb.append(s);
                    inputCode10.clearFocus();
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputCode10.length() < 1) {
                    inputCode10.requestFocus();
                }
            }
        });*/

        editGoodNews.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (editGoodNews.length() != 0) {
                    sb.append(s);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editGoodNews.length() < 1) {
                    editGoodNews.requestFocus();
                }
            }
        });

        inputAddressStore.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (inputAddressStore.length() != 0) {
                    sb.append(s);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (inputAddressStore.length() < 1) {
                    inputAddressStore.requestFocus();
                }
            }
        });

        textStore.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                cekInput();
                if (textStore.length() != 0) {
                    sb.append(s);
                    cekInput();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (sb.length() == 1) {
                    sb.deleteCharAt(0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (textStore.length() < 1) {
                    textStore.requestFocus();
                }
            }
        });

        /*spinnerKodeProduksi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cekInput();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/
    }

    public void cekInput() {
        if (brands != null
                && product != null
                && layoutAddPhoto.getVisibility() == View.GONE
                && ((report == false
                /*&& inputCode1.length() != 0
                && inputCode2.length() != 0
                && inputCode3.length() != 0
                && inputCode4.length() != 0
                && inputCode5.length() != 0
                && inputCode6.length() != 0
                && inputTime1.length() != 0
                && inputTime2.length() != 0*/
                && inputAddressStore.length() != 0
                && spinCity.getSelectedItemPosition() != 0
                && spinProv.getSelectedItemPosition() != 0
                //&& spinnerKodeProduksi.getSelectedItemPosition() != 0
                && textStore.length() != 0
                && (partCondition != null || textMore != ""))
                || (report && editGoodNews.getText().length() != 0))
                ) {
            /*prodCode = inputCode1.getText().toString()
                    + inputCode2.getText().toString()
                    + inputCode3.getText().toString()
                    + inputCode4.getText().toString()
                    + inputCode5.getText().toString()
                    + inputCode6.getText().toString()
                    + inputTime1.getText().toString()
                    + inputTime2.getText().toString()
                    + spinnerKodeProduksi.getSelectedItem().toString()
                    + inputCode7.getText().toString()
                    + inputCode8.getText().toString()
                    + inputCode9.getText().toString()
                    + inputCode10.getText().toString();*/
            enable();
        } else {
            disable();
        }
    }

    private void clear() {
        brands = null;
        product = null;
        layoutAddPhoto.setVisibility(View.VISIBLE);
        imgBackground.setImageBitmap(null);
        imagePhoto.setImageResource(R.drawable.icn_addphoto);
        /*inputCode1.setText("");
        inputCode2.setText("");
        inputCode3.setText("");
        inputCode4.setText("");
        inputCode5.setText("");
        inputCode6.setText("");
        inputTime1.setText("");
        inputTime2.setText("");
        inputCode7.setText("");
        inputCode8.setText("");
        inputCode9.setText("");
        inputCode10.setText("");
        prodCode = "";*/
        inputAddressStore.setText("");
        textStore.setText("");
        adapterKodeProduksi.clear();
        //spinnerKodeProduksi.setSelection(0);
        layoutBadReport.setVisibility(View.GONE);
        layoutGoodReport.setVisibility(View.GONE);
        for (int i = 0; i < dataResource.getBrands().size(); i++) {
            dataResource.getBrands().get(i).setSelected(true);
        }
        for (int i = 0; i < dataResource.getProducts().size(); i++) {
            dataResource.getProducts().get(i).setSelected(false);
        }
        for (int i = 0; i < dataResource.getVendorTypes().size(); i++) {
            dataResource.getVendorTypes().get(i).setSelected(false);
        }
        for (int i = 0; i < dataResource.getParts().size(); i++) {
            dataResource.getParts().get(i).setSelected(false);
        }
        for (int i = 0; i < dataResource.getConditions().size(); i++) {
            dataResource.getConditions().get(i).setSelected(false);
        }
        textListProduct.setText(getResources().getString(R.string.addReport_list_task_product));
        showData(dataResource);
        editGoodNews.setText("");
        imgReportGood.setImageResource(R.mipmap.btn_danone_thumb_up_off);
        imgReportBad.setImageResource(R.mipmap.btn_danone_thumb_down_off);
        layoutBadReport.setVisibility(View.GONE);
        productRCAdapter.clear();
        textReportGood.setTextColor(getResources().getColor(R.color.text_grey_quality_survey));
        textReportBad.setTextColor(getResources().getColor(R.color.text_grey_quality_survey));
        partRCAdapter.clear();
        imgProduct.setImageBitmap(null);
    }

    public void disable() {
        buttonLogin.setEnabled(false);
        cardView.setCardBackgroundColor(CommonUtils.getColor(getActivity(), R.color.text_grey_quality_survey));
    }

    public void enable() {
        buttonLogin.setEnabled(true);
        cardView.setCardBackgroundColor(CommonUtils.getColor(getActivity(), R.color.nutricia_purple));
    }

    @Override
    public void selectBrand(int selectedPos, List<Brand> listTask) {
        brandRCAdapter.notifyDataSetChanged();
        productRCAdapter.clear();
        kodeProd.clear();
        String idBrand = listTask.get(selectedPos).getId();
        for (int i = 0; i < dataResource.getProducts().size(); i++) {
            if (dataResource.getProducts().get(i).getBrand().equals(idBrand)) {
                productRCAdapter.add(dataResource.getProducts().get(i));
            }
        }
        for (int i = 0; i < dataResource.getCodeProduction().size(); i++) {
            if (dataResource.getCodeProduction().get(i).getBrand().equals(idBrand)) {
                kodeProd.add(dataResource.getCodeProduction().get(i).getCode());
            }
        }
        textListProduct.setText(getResources().getString(R.string.addReport_list_task_product));
        adapterKodeProduksi = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kodeProd);
        adapterKodeProduksi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spinnerKodeProduksi.setAdapter(adapterKodeProduksi);
    }

    @Override
    public void selectProduct(int selectedPos, List<Product> listProduct) {
        productRCAdapter.notifyDataSetChanged();
        dialog.dismiss();
        textListProduct.setText(listProduct.get(selectedPos).getName());
        Glide.with(getActivity()).load(listProduct.get(selectedPos).getImage()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgProduct);

        partRCAdapter.clear();
        String idProduct = listProduct.get(selectedPos).getId();
        for (int i = 0; i < dataResource.getParts().size(); i++) {
            if (dataResource.getParts().get(i).getProduct().equals(idProduct) && dataResource.getParts().get(i).getType().equals("feedback")) {
                partRCAdapter.add(dataResource.getParts().get(i));
            }
        }
        rvPart.setUpAsList();
        rvPart.setAdapter(partRCAdapter);
    }

    @Override
    public void selectPart(int selectedPos, List<Part> listPart) {
        partRCAdapter.notifyDataSetChanged();
    }

    @Override
    public void selectCondition(int selectedPos, List<Condition> listCondition) {
        partConditionRCAdapter.notifyDataSetChanged();
    }

    @Override
    public void selectVendor(int selectedPos, List<VendorType> listVendor) {
        vendorRCAdapter.notifyDataSetChanged();

        if (listVendor.get(selectedPos).getName().equals(getActivity().getResources().getString(R.string.addReport_modern))) {
            modernTrade.clear();
            for (int i = 0; i < dataResource.getTrades().size(); i++) {
                modernTrade.add(dataResource.getTrades().get(i).getName());
            }
            textStore.setAdapter(adapterTrade);
            textStore.setThreshold(1);
        } else {
            textStore.setAdapter(null);
        }
    }

    @Override
    public void successSubmit() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success_submit);
        TextView textDone = ButterKnife.findById(dialog, R.id.text_done);
        clear();
        textDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(getActivity() instanceof HomeActivity){
                    HomeActivity myactivity = (HomeActivity) getActivity();
                    myactivity.showHistory();
                }
            }
        });
        dialog.show();
    }

    public void showProduct() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_product);
        CustomRecycleView rvProduct = ButterKnife.findById(dialog, R.id.rv_product);
        rvProduct.setAdapter(productRCAdapter);
        rvProduct.setUpAsList();
        dialog.show();

    }

    @OnClick(R.id.layout_product)
    public void onClick() {
        showProduct();
    }

    private void dialogCondition(String name, final int position) {
        dialogCondition = new Dialog(getActivity());
        dialogCondition.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCondition.setContentView(R.layout.dialog_condition);
        rvCondition = ButterKnife.findById(dialogCondition, R.id.rv_condition);
        TextView textPart = ButterKnife.findById(dialogCondition, R.id.text_part);
        final EditText inputMore = ButterKnife.findById(dialogCondition, R.id.input_more);
        textPart.setText(name);
        inputMore.setText(textMore);
        text_done = ButterKnife.findById(dialogCondition, R.id.text_selesai);
        rvCondition.setUpAsList();
        rvCondition.setAdapter(partConditionRCAdapter);
        text_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textMore = inputMore.getText().toString();
                dialogCondition.dismiss();
                mainPresenter.selectPart(position, partRCAdapter.getData());
                cekInput();
                part = partRCAdapter.getData().get(position);
            }
        });
        dialogCondition.show();
    }


    private void getloc() {
        try {
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        // don't start listeners if no provider is enabled
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Attention!");
            builder.setMessage("Sorry, location is not determined. Please enable location providers");
            builder.create().show();
        }

        if (gps_enabled) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        123);
                return;
            }
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
        }
        if (network_enabled) {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 123:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
                } else {
                    CommonUtils.showToast(getActivity(), "Some Permission is Denied");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    @OnClick(R.id.button_login)
    public void onSubmit() {
        showDialogSum();
    }

    class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                locManager.removeUpdates(locListener);
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();

                    sCity.clear();
                    sCity.add("Jakarta");
                    sCity.add(city);
                    if(sCity.size() > 1){
                        sCity.remove(0);
                    }
                    ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_item, sCity);
                    cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinCity.setAdapter(cityAdapter);

                    sProvince.clear();
                    sProvince.add("DKI Jakarta");
                    sProvince.add(state);
                    if (sProvince.size() > 1){
                        sProvince.remove(0);
                    }
                    ArrayAdapter<String> provAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_item, sProvince);
                    provAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinProv.setAdapter(provAdapter);

                    spinCity.setSelection(1);
                    spinProv.setSelection(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private void showDialogSum(){
        dialogSummary = new Dialog(getActivity());
        dialogSummary.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSummary.setContentView(R.layout.dialog_submit);

        LinearLayout layoutBad = ButterKnife.findById(dialogSummary, R.id.layout_bad);
        LinearLayout layoutGood = ButterKnife.findById(dialogSummary, R.id.layout_good);
        TextView textSubmit = ButterKnife.findById(dialogSummary, R.id.text_submit);
        // layout good
        TextView textBrandGood = ButterKnife.findById(dialogSummary, R.id.text_Brand_good);
        TextView textProductGood = ButterKnife.findById(dialogSummary, R.id.text_product_good);
        ImageView imgBrandGood = ButterKnife.findById(dialogSummary, R.id.img_brand_good);
        TextView textDateGood = ButterKnife.findById(dialogSummary, R.id.text_date_good);
        ImageView imgPhotoGood = ButterKnife.findById(dialogSummary, R.id.img_photo_good);
        // layout bad
        TextView textStoreBad = ButterKnife.findById(dialogSummary, R.id.text_store);
        TextView textAddressBad = ButterKnife.findById(dialogSummary, R.id.text_address);
        TextView textBrandBad = ButterKnife.findById(dialogSummary, R.id.text_Brand);
        TextView textProductBad = ButterKnife.findById(dialogSummary, R.id.text_product);
        TextView textPartBad = ButterKnife.findById(dialogSummary, R.id.text_part);
        TextView textPartConditionBad = ButterKnife.findById(dialogSummary, R.id.text_part_condition);
        TextView textProdCodeBad = ButterKnife.findById(dialogSummary, R.id.text_prod_code);
        TextView textDateBad = ButterKnife.findById(dialogSummary, R.id.text_date);
        ImageView imgBrandBad = ButterKnife.findById(dialogSummary, R.id.img_brand);
        ImageView imgPhotoBad = ButterKnife.findById(dialogSummary, R.id.img_photo);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        String formattedDate = df.format(c.getTime());
        final Vendor vendor = new Vendor();
        final List<Conditions> conditions = new ArrayList<>();
        if (layoutBadReport.getVisibility() == View.VISIBLE){
            layoutBad.setVisibility(View.VISIBLE);
            layoutGood.setVisibility(View.GONE);
            textStoreBad.setText(textStore.getText().toString());
            textAddressBad.setText(inputAddressStore.getText().toString());
            textBrandBad.setText(brands.getName());
            textProductBad.setText(product.getName());
            textPartBad.setText(part.getName());
            textPartConditionBad.setText(partCondition.getDescription());
            textProdCodeBad.setText("");
            textDateBad.setText(formattedDate);
            if (brands.getImage() != ""){
                Glide.with(this).load(brands.getImage()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgBrandBad);
            }
            imgPhotoBad.setImageBitmap(((BitmapDrawable)imagePhoto.getDrawable()).getBitmap());
            Conditions conditions1 = new Conditions();
            conditions1.setId(Integer.parseInt(partCondition.getId()));
            conditions1.setDescription(partCondition.getDescription());
            conditions.add(conditions1);
            vendor.setType(vendort.getName());
            vendor.setName(textStore.getText().toString());
            vendor.setAddress(inputAddressStore.getText().toString());
            if ((sProvince.size() != 0 && sCity.size() != 0) || (!sProvince.isEmpty() && !sCity.isEmpty())){
                vendor.setCity(spinCity.getSelectedItem().toString());
                vendor.setProvince(spinProv.getSelectedItem().toString());
            }else {
                vendor.setCity("Jakarta");
                vendor.setProvince("DKI Jakarta");
            }

        }
        else if (layoutGoodReport.getVisibility() == View.VISIBLE){
            layoutBad.setVisibility(View.GONE);
            layoutGood.setVisibility(View.VISIBLE);
            textBrandGood.setText(brands.getName());
            textProductGood.setText(editGoodNews.getText().toString());
            textDateGood.setText(formattedDate);
            if (brands.getImage() != ""){
                Glide.with(this).load(brands.getImage()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgBrandGood);
            }
            imgPhotoGood.setImageBitmap(((BitmapDrawable)imagePhoto.getDrawable()).getBitmap());
        }

        textSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSummary.dismiss();
                mainPresenter.submitFeedback(Integer.parseInt(brands.getId()), Integer.parseInt(product.getId()), report, product.getName(), encodeToBase64(((BitmapDrawable)imagePhoto.getDrawable()).getBitmap()), editGoodNews.getText().toString(), "", conditions, vendor);
            }
        });

        dialogSummary.show();

    }

}
