package com.ntk.home.news;

import com.ntk.data.model.ListNews;

import java.util.List;

/**
 * Created by Dycode on 18/10/2016.
 */

public interface NewsView {
    void showLoad();
    void dissmissLoad();
    void showError(String string);
    void showDataNews(List<ListNews> listNewses);
    void showNullNews();
    void showDataFilter(List<ListNews> listNewses);
    void onAddBookmark(String idNews, String title, Boolean add);
    void addBookmark();
}
