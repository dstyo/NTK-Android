package com.ntk.home.news;

import com.ntk.data.ApiService;
import com.ntk.data.model.ActionBookmark;
import com.ntk.data.model.ListNews;
import com.ntk.data.model.ReturnListNews;
import com.ntk.data.model.ReturnString;
import com.ntk.data.preference.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dycode on 18/10/2016.
 */

public class NewsPresenter {
    ApiService apiService;
    NewsView newsView;

    List<ListNews> listNewses;

    public NewsPresenter(ApiService apiService, NewsView newsView){
        this.apiService = apiService;
        this.newsView = newsView;
    }

    public void getListNews(){
        newsView.showLoad();
        apiService.getListNews(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnListNews>>) new Func1<Throwable, Observable<? extends ReturnListNews>>() {
                    @Override
                    public Observable<? extends ReturnListNews> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnListNews>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsView.showError(e.getMessage());
                        newsView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnListNews resource) {
                        if(resource.getData().isEmpty()){
                            newsView.showNullNews();
                            newsView.dissmissLoad();
                        }else{
                            newsView.showDataNews(resource.getData());
                            listNewses = resource.getData();
                            newsView.dissmissLoad();
                        }


                    }
                });
    }

    public void filterNews(String string, Boolean bookmark){
        List<ListNews> newses = new ArrayList<>();
        for (int i = 0; i < listNewses.size(); i++){
            if (bookmark){
                if (listNewses.get(i).getTitle().toLowerCase().contains(string) && listNewses.get(i).getBookmarked()){
                    newses.add(listNewses.get(i));
                }
            }
            else {
                if (listNewses.get(i).getTitle().toLowerCase().contains(string)){
                    newses.add(listNewses.get(i));
                }
            }

        }
        newsView.showDataFilter(newses);
    }

    public void bookmark(Boolean bookmark){
        List<ListNews> newses = new ArrayList<>();
        for (int i = 0; i < listNewses.size(); i++){
            if (bookmark){
                if (listNewses.get(i).getBookmarked()){
                    newses.add(listNewses.get(i));
                }
            }
            else {
                newses.add(listNewses.get(i));
            }
        }
        newsView.showDataFilter(newses);
    }

    public void addBookmark(String id){
        for (int i = 0; i < listNewses.size(); i++){
            if (listNewses.get(i).getId().equals(id)){
                if (listNewses.get(i).getBookmarked()){
                    ActionBookmark actionBookmark = new ActionBookmark();
                    actionBookmark.setAction("unbookmark");
                    bookmark(id, actionBookmark);
                }
                else {
                    ActionBookmark actionBookmark = new ActionBookmark();
                    actionBookmark.setAction("bookmark");
                    bookmark(id, actionBookmark);
                }
            }
        }
    }

    private void bookmark(final String id, final ActionBookmark actionBookmark){
        newsView.showLoad();
        apiService.Bookmark(id, PreferenceHelper.getUserData().getAccessToken(), actionBookmark)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsView.showError(e.getMessage());
                        newsView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnString resource) {
                        newsView.dissmissLoad();
                        getListNews();
                    }
                });
    }
}
