package com.ntk.home.history;

import com.ntk.data.ApiService;
import com.ntk.data.model.history.Datum;
import com.ntk.data.model.history.ReturnHistory;
import com.ntk.data.preference.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dycode on 26/10/2016.
 */

public class HistoryPresenter {
    ApiService apiService;
    HistoryView historyView;
    List<Datum> historys = new ArrayList<>();

    public HistoryPresenter(ApiService apiService, HistoryView historyView){
        this.apiService = apiService;
        this.historyView = historyView;
    }

    public void getDataHistory(){
        historyView.showLoad();
        apiService.getHistory(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnHistory>>) new Func1<Throwable, Observable<? extends ReturnHistory>>() {
                    @Override
                    public Observable<? extends ReturnHistory> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnHistory>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        historyView.showError(e.getMessage());
                        historyView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnHistory resource) {
                        if(resource.getData().isEmpty()){
                            historyView.showNullData();
                            historyView.dissmissLoad();
                        }else{
                            historyView.showDataNews(resource.getData());
                            historys = resource.getData();
                            historyView.dissmissLoad();
                        }
                    }
                });
    }

    public void selectHistory(String id){
        for (int i = 0; i < historys.size(); i++){
            for (int a = 0; a < historys.get(i).getHistory().size(); a++){
                if (historys.get(i).getHistory().get(a).getId().equals(id) && !historys.get(i).getHistory().get(a).getSelected()){
                    historys.get(i).getHistory().get(a).setSelected(true);
                }
                else {
                    historys.get(i).getHistory().get(a).setSelected(false);
                }
            }
        }
        historyView.showDataNews(historys);
    }
}
