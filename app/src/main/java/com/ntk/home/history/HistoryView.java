package com.ntk.home.history;

import com.ntk.data.model.history.Datum;

import java.util.List;

/**
 * Created by Dycode on 26/10/2016.
 */

public interface HistoryView {
    void showLoad();
    void dissmissLoad();
    void showError(String string);
    void showDataNews(List<Datum> data);
    void showNullData();
    void selected();
}
