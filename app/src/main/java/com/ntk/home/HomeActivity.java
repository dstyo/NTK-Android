package com.ntk.home;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ntk.NewsDetail.NewsDetailActivity;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.base.BaseActivity;
import com.ntk.constant.UrlConstant;
import com.ntk.data.ApiService;
import com.ntk.data.preference.PreferenceHelper;
import com.ntk.home.adapter.HomeTabAdapter;
import com.ntk.home.fragment.MainFragment;
import com.ntk.home.fragment.ProfileFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.26.02
 */
public class HomeActivity extends BaseActivity implements IHomeView {

    @BindView(R.id.tab_home)
    TabLayout tabHome;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    HomeTabAdapter homeTabAdapter;
    @Inject
    ApiService apiService;

    MainFragment mainFragment;
    ProfileFragment profileFragment;
    //NewsFragment newsFragment;
    HomePresenter homePresenter;

    TabLayout.Tab history, addReport,  profile;
    String notif = "", idNews = "";
    Boolean unread = false;
    @Override
    protected int getActivityView() {
        return R.layout.activity_home;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {

        ((QualityFeedbackApp)this.getApplication())
                .getComponent()
                .inject(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            notif = bundle.getString("notif");
            idNews = bundle.getString("idNews");
        }

        homeTabAdapter = new HomeTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(homeTabAdapter);
        tabHome.setupWithViewPager(viewPager);
        mainFragment = (MainFragment) homeTabAdapter.getItem(1);
        profileFragment = (ProfileFragment) homeTabAdapter.getItem(2);
        //newsFragment = (NewsFragment) homeTabAdapter.getItem(2);

        history = tabHome.getTabAt(0);
        addReport = tabHome.getTabAt(1);
        //news = tabHome.getTabAt(2);
        profile = tabHome.getTabAt(2);

        homePresenter = new HomePresenter(this, apiService);

        homePresenter.checkNewsUnread();
        if (notif.equals("history")){
            tabHome.getChildAt(0);
            viewPager.setCurrentItem(0);

            history.setCustomView(R.layout.custom_tab);
            history.setText("history");
            history.setIcon(R.mipmap.img_danone_tab_history_on);

            addReport.setCustomView(R.layout.custom_tab);
            addReport.setText("add report");
            addReport.setIcon(R.mipmap.img_danone_tab_add_report_off);

//            news.setCustomView(R.layout.custom_tab);
//            news.setText("news");
//            if (unread){
//                news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//            }else {
//                news.setIcon(R.mipmap.img_danone_tab_news_off);
//            }

            profile.setCustomView(R.layout.custom_tab);
            profile.setText("profile");
            profile.setIcon(R.mipmap.img_danone_tab_profile_off);
        }
        else {
            homePresenter.checkDialog(idNews);
            tabHome.getChildAt(1);
            viewPager.setCurrentItem(1);
            history.setCustomView(R.layout.custom_tab);
            history.setText(getResources().getString(R.string.home_tab_history));
            history.setIcon(R.mipmap.img_danone_tab_history_off);

            addReport.setCustomView(R.layout.custom_tab);
            addReport.setText(getResources().getString(R.string.home_tab_add_report));
            addReport.setIcon(R.mipmap.img_danone_tab_add_report_on);

//            news.setCustomView(R.layout.custom_tab);
//            news.setText(getResources().getString(R.string.home_tab_news));
//            if (unread){
//                news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//            }else {
//                news.setIcon(R.mipmap.img_danone_tab_news_off);
//            }

            profile.setCustomView(R.layout.custom_tab);
            profile.setText(getResources().getString(R.string.home_tab_profile));
            profile.setIcon(R.mipmap.img_danone_tab_profile_off);
        }
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                homePresenter.checkNewsUnread();
                switch (position){
                    case 0:
                        history.setIcon(R.mipmap.img_danone_tab_history_on);
                        addReport.setIcon(R.mipmap.img_danone_tab_add_report_off);
//                        if (unread){
//                            news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//                        }else {
//                            news.setIcon(R.mipmap.img_danone_tab_news_off);
//                        }
                        profile.setIcon(R.mipmap.img_danone_tab_profile_off);
                        break;
                    case 1:
                        history.setIcon(R.mipmap.img_danone_tab_history_off);
                        addReport.setIcon(R.mipmap.img_danone_tab_add_report_on);
//                        if (unread){
//                            news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//                        }else {
//                            news.setIcon(R.mipmap.img_danone_tab_news_off);
//                        }
                        profile.setIcon(R.mipmap.img_danone_tab_profile_off);
                        break;
                    case 2:
                        history.setIcon(R.mipmap.img_danone_tab_history_off);
                        addReport.setIcon(R.mipmap.img_danone_tab_add_report_off);
//                        if (unread){
//                            news.setIcon(R.mipmap.img_danone_tab_news_on_unread);
//                        }else {
//                            news.setIcon(R.mipmap.img_danone_tab_news_on);
//                        }
                        profile.setIcon(R.mipmap.img_danone_tab_profile_off);
                        break;
                    case 3:
                        history.setIcon(R.mipmap.img_danone_tab_history_off);
                        addReport.setIcon(R.mipmap.img_danone_tab_add_report_off);
//                        if (unread){
//                            news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//                        }else {
//                            news.setIcon(R.mipmap.img_danone_tab_news_off);
//                        }
                        profile.setIcon(R.mipmap.img_danone_tab_profile_on);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private Bitmap resizeBitmap(Bitmap bitmap) {
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 144, getResources().getDisplayMetrics());
        float scale = (float) bitmap.getHeight() / bitmap.getWidth();
        return Bitmap.createScaledBitmap(bitmap, size, (int) (size * scale), false);
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void dissmissLoad() {

    }

    @Override
    public void showDialog(final String idNews){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_news);
        WebView webView = ButterKnife.findById(dialog, R.id.webView);
        ImageView imgClose = ButterKnife.findById(dialog, R.id.img_close);
        TextView textMore = ButterKnife.findById(dialog, R.id.text_more);
        final ProgressBar progress = ButterKnife.findById(dialog, R.id.progressBar);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(UrlConstant.ROOT + "api/v1/news/" + idNews + "?access_token=" + PreferenceHelper.getUserData().getAccessToken());

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started
                // Visible the progressbar
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url){
                // Do something when page loading finished
            }

        });

        webView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int newProgress){
                // Update the progress bar with page loading progress
                progress.setProgress(newProgress);
                if(newProgress == 100){
                    // Hide the progressbar
                    progress.setVisibility(View.GONE);
                }
            }
        });

        textMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Intent intent = new Intent(Activity, NewsDetailActivity.class);
                intent.putExtra("idNews", idNews);
                startActivityForResult(intent, 2);
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    @Override
    public void unRead(Boolean unread) {
        this.unread = unread;
//        if (unread){
//            news.setIcon(R.mipmap.img_danone_tab_news_off_unread);
//        }else {
//            news.setIcon(R.mipmap.img_danone_tab_news_off);
//        }
        //newsFragment.refresh();
    }

    public void showHistory(){
        tabHome.getChildAt(0);
        viewPager.setCurrentItem(0);

    }
}