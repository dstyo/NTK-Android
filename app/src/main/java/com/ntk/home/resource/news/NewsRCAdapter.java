package com.ntk.home.resource.news;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.ListNews;
import com.ntk.home.news.NewsView;

/**
 * Created by Dycode on 17/10/2016.
 */

public class NewsRCAdapter extends BaseAdapterItemReyclerView<ListNews, NewsViewHolder> {

    NewsView newsView;
    public NewsRCAdapter(Context mContext, NewsView newsView) {
        super(mContext);
        this.newsView = newsView;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_news;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, newsView);
    }


}
