package com.ntk.home.resource.partcondition;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.resource.Condition;
import com.ntk.home.main.IMainView;

import butterknife.BindView;

/**
 * Created by Dycode on 03/10/2016.
 */

public class PartConditionViewHolder extends BaseItemRecyclerViewHolder<Condition> {

    IMainView task2View;
    @BindView(R.id.text_condition)
    TextView textCondition;
    @BindView(R.id.img_condition)
    ImageView imgCondition;

    public PartConditionViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, IMainView task2View) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.task2View = task2View;
    }

    @Override
    public void bind(Condition partCondition) {
        textCondition.setText(partCondition.getDescription());
        if (partCondition.getSelected()){
            imgCondition.setImageResource(R.mipmap.btn_danone_radio_button_on);
        }
        else {
            imgCondition.setImageResource(R.mipmap.btn_danone_radio_button_off);
        }
    }
}
