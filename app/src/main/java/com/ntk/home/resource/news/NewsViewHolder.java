package com.ntk.home.resource.news;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.ListNews;
import com.ntk.home.news.NewsView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by Dycode on 17/10/2016.
 */

public class NewsViewHolder extends BaseItemRecyclerViewHolder<ListNews> {
    NewsView newsView;
    @BindView(R.id.img_news)
    ImageView imgNews;
    @BindView(R.id.text_title)
    TextView textTitle;
    @BindView(R.id.text_date)
    TextView textDate;
    @BindView(R.id.img_bookmark)
    ImageView imgBookmark;
    @BindView(R.id.img_unread)
    ImageView imgUnRead;
    @BindView(R.id.linearLogin)
    CardView linearLogin;

    public NewsViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, NewsView newsView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.newsView = newsView;
    }

    @Override
    public void bind(final ListNews listNews) {
        textTitle.setText(listNews.getTitle());
        Date newDate = null;
        String date = listNews.getCreatedAt();
        String strCurrentDate = date.substring(0, 10);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("dd MMM, yyyy");
        String dates = format.format(newDate);


        textDate.setText(dates);
        if (!listNews.getPhoto().equals("")){
            Glide.with(mContext).load(listNews.getPhoto()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgNews);
        }
        if (listNews.getBookmarked()){
            imgBookmark.setImageResource(R.mipmap.favorite_grey_on);
        }
        else {
            imgBookmark.setImageResource(R.mipmap.favorite_grey_off);
        }

        if (listNews.getRead()){
            imgUnRead.setVisibility(View.GONE);
            textTitle.setTypeface(null, Typeface.NORMAL);
        }
        else {
            imgUnRead.setVisibility(View.VISIBLE);
            textTitle.setTypeface(null, Typeface.BOLD);
        }

        imgBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsView.onAddBookmark(listNews.getId(), listNews.getTitle(), listNews.getBookmarked());
            }
        });
    }
}
