package com.ntk.home.resource.partcondition;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.resource.Condition;
import com.ntk.home.main.IMainView;

/**
 * Created by Dycode on 03/10/2016.
 */

public class PartConditionRCAdapter extends BaseAdapterItemReyclerView<Condition, PartConditionViewHolder> {

    IMainView task2View;
    public PartConditionRCAdapter(Context mContext, IMainView task2View) {
        super(mContext);
        this.task2View = task2View;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_condition;
    }

    @Override
    public PartConditionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PartConditionViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, task2View);
    }
}
