package com.ntk.home.resource.brand;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.resource.Brand;
import com.ntk.home.main.IMainView;
import com.ntk.utils.CommonUtils;

import butterknife.BindView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class BrandViewHolder extends BaseItemRecyclerViewHolder<Brand> {

    @BindView(R.id.image_brand)
    ImageView imageBrand;
    @BindView(R.id.image_brand_tint)
    ImageView imageBrandTint;
    @BindView(R.id.layout_brand)
    LinearLayout layoutBrand;

    IMainView iMainView;

    public BrandViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, IMainView iMainView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.iMainView = iMainView;
    }

    @Override
    public void bind(Brand brand) {
        if (!brand.getImage().equals("")){
            Glide.with(mContext).load(brand.getImage()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageBrand);
            Glide.with(mContext).load(brand.getImage()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    Bitmap BandW = CommonUtils.createContrast(resource, 50);
                    imageBrandTint.setImageBitmap(BandW);
                }
            });
        }

        if (!brand.getSelected()){
            imageBrand.setVisibility(View.GONE);
            imageBrandTint.setVisibility(View.VISIBLE);
        }
        else {
            imageBrand.setVisibility(View.VISIBLE);
            imageBrandTint.setVisibility(View.GONE);
        }
    }
}
