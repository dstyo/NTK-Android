package com.ntk.home.resource.part;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.resource.Part;
import com.ntk.home.main.IMainView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class PartRCAdapter extends BaseAdapterItemReyclerView<Part, PartViewHolder> {

    IMainView iMainView;
    public PartRCAdapter(Context mContext, IMainView iMainView) {
        super(mContext);
        this.iMainView = iMainView;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_part;
    }

    @Override
    public PartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PartViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, iMainView);
    }
}
