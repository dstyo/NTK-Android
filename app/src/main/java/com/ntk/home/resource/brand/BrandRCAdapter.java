package com.ntk.home.resource.brand;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.resource.Brand;
import com.ntk.home.main.IMainView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class BrandRCAdapter extends BaseAdapterItemReyclerView<Brand, BrandViewHolder> {

    IMainView iMainView;
    public BrandRCAdapter(Context mContext, IMainView iMainView) {
        super(mContext);
        this.iMainView = iMainView;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_brand;
    }

    @Override
    public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BrandViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, iMainView);
    }
}
