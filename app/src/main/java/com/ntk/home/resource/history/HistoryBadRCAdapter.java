package com.ntk.home.resource.history;

import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.history.History;

/**
 * Created by Dycode on 26/10/2016.
 */

public class HistoryBadRCAdapter extends BaseAdapterItemReyclerView<History, HistoryBadViewHolder> {

    Resources res;
    public HistoryBadRCAdapter(Context mContext, Resources res) {
        super(mContext);
        this.res = res;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_history_detail;
    }

    @Override
    public HistoryBadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryBadViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, res);
    }
}