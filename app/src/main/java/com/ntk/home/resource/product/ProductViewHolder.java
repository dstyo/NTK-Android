package com.ntk.home.resource.product;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.resource.Product;
import com.ntk.home.main.IMainView;

import butterknife.BindView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class ProductViewHolder extends BaseItemRecyclerViewHolder<Product> {
    IMainView iMainView;
    @BindView(R.id.text_product)
    TextView textProduct;
    @BindView(R.id.img_product_rb)
    ImageView imgProductRb;

    public ProductViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, IMainView iMainView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.iMainView = iMainView;
    }

    @Override
    public void bind(Product product) {
        textProduct.setText(product.getName());
        if (product.getSelected()){
            imgProductRb.setImageResource(R.mipmap.btn_danone_radio_button_on);
        }
        else {
            imgProductRb.setImageResource(R.mipmap.btn_danone_radio_button_off);
        }

    }
}
