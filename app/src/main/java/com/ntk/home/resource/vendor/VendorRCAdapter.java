package com.ntk.home.resource.vendor;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.resource.VendorType;
import com.ntk.home.main.IMainView;

/**
 * Created by Dycode on 03/10/2016.
 */

public class VendorRCAdapter extends BaseAdapterItemReyclerView<VendorType, VendorViewHolder> {

    IMainView iMainView;
    public VendorRCAdapter(Context mContext, IMainView iMainView) {
        super(mContext);
        this.iMainView = iMainView;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_vendor_type;
    }

    @Override
    public VendorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VendorViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, iMainView);
    }
}
