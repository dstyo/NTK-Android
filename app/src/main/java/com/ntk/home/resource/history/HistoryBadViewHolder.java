package com.ntk.home.resource.history;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.history.History;

import butterknife.BindView;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.28.02
 */

public class HistoryBadViewHolder extends BaseItemRecyclerViewHolder<History> {

    @BindView(R.id.img_bad)
    ImageView imgBad;
    @BindView(R.id.text_bad)
    TextView textBad;
    @BindView(R.id.text_date)
    TextView textDate;
    @BindView(R.id.img_news)
    ImageView imgNews;
    @BindView(R.id.text_address)
    TextView textAddress;
    @BindView(R.id.text_store)
    TextView textStore;
    @BindView(R.id.view_good)
    View viewGood;
    @BindView(R.id.view_bad)
    View viewBad;
    @BindView(R.id.text_sku)
    TextView textSku;
    @BindView(R.id.text_part)
    TextView textPart;
    @BindView(R.id.text_condition)
    TextView textCondition;
    @BindView(R.id.layout_detail)
    LinearLayout layoutDetail;
    @BindView(R.id.linearLogin)
    CardView linearLogin;

    Resources res;

    public HistoryBadViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, Resources res) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.res = res;
    }

    @Override
    public void bind(History history) {
        if (!history.getPhoto().equals("")) {
            Glide.with(mContext).load(history.getPhoto()).asBitmap().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imgNews);
        }

        textSku.setText(history.getSku());
        textDate.setText(history.getTimePost());

        if (history.getIsGood()){
            textBad.setText(res.getString(R.string.addReport_title_type_good));
            imgBad.setImageResource(R.drawable.btn_beritabaikaktif);
            textAddress.setVisibility(View.GONE);
            textStore.setVisibility(View.GONE);
            textCondition.setVisibility(View.GONE);
            textPart.setText(history.getDescription());
            viewBad.setVisibility(View.GONE);
            viewGood.setVisibility(View.VISIBLE);
        }
        else {
            textBad.setText(res.getString(R.string.addReport_title_type_bad));
            imgBad.setImageResource(R.drawable.btn_beritaburukaktif);
            textAddress.setVisibility(View.VISIBLE);
            textStore.setVisibility(View.VISIBLE);
            textAddress.setText(history.getVendor().getVendorAddress());
            textStore.setText(history.getVendor().getVendorName());
            textCondition.setVisibility(View.VISIBLE);
            textPart.setText(history.getConditions().get(0).getPart());
            textCondition.setText(history.getConditions().get(0).getDescription());
            viewBad.setVisibility(View.VISIBLE);
            viewGood.setVisibility(View.GONE);
        }

//        if (history.getSelected()) {
//            if (layoutDetail.getVisibility() == View.VISIBLE) {
//                LayoutUtils.hideExpandable(layoutDetail);
//            } else {
//                LayoutUtils.showExpandable(layoutDetail);
//            }
//
//        } else {
//            layoutDetail.setVisibility(View.GONE);
//        }
    }
}
