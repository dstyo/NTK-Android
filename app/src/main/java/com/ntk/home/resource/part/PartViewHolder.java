package com.ntk.home.resource.part;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.resource.Part;
import com.ntk.home.main.IMainView;
import com.ntk.utils.CommonUtils;

import butterknife.BindView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class PartViewHolder extends BaseItemRecyclerViewHolder<Part> {
    IMainView iMainView;
    @BindView(R.id.text_part)
    TextView textPart;
    @BindView(R.id.layout_part)
    CardView layoutPart;

    public PartViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, IMainView iMainView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.iMainView = iMainView;
    }

    protected int getItemView(int viewType) {
        return R.layout.item_part;
    }

    @Override
    public void bind(Part part) {
        textPart.setText(part.getName());

        if (part.getSelected()){
            layoutPart.setCardBackgroundColor(CommonUtils.getColor(mContext, R.color.color_light_green_quality_survey));
        }
        else {
            layoutPart.setCardBackgroundColor(CommonUtils.getColor(mContext, R.color.white_quality_survey));
        }
    }
}
