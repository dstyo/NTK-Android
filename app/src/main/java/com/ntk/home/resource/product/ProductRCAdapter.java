package com.ntk.home.resource.product;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.resource.Product;
import com.ntk.home.main.IMainView;

/**
 * Created by Dean Nurchusnul on 23/09/2016.
 */
public class ProductRCAdapter extends BaseAdapterItemReyclerView<Product, ProductViewHolder> {

        IMainView iMainView;

    public ProductRCAdapter(Context mContext, IMainView iMainView) {
        super(mContext);
        this.iMainView = iMainView;
    }
    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_product;
        }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, iMainView);
        }
}
