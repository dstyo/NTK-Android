package com.ntk.home.resource.vendor;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.resource.VendorType;
import com.ntk.home.main.IMainView;

import butterknife.BindView;

/**
 * Created by Dycode on 03/10/2016.
 */

public class VendorViewHolder extends BaseItemRecyclerViewHolder<VendorType>{
    IMainView iMainView;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.text_description)
    TextView textDescription;
    @BindView(R.id.img_type_vendor)
    ImageView imgTypeVendor;

    public VendorViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, IMainView iMainView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.iMainView = iMainView;
    }

    @Override
    public void bind(VendorType vendorType) {
        textName.setText(vendorType.getName());
        textDescription.setText(vendorType.getDescription());
        if (vendorType.getSelected()){
            imgTypeVendor.setImageResource(R.mipmap.btn_danone_radio_button_on);
        }
        else {
            imgTypeVendor.setImageResource(R.mipmap.btn_danone_radio_button_off);
        }
    }
}
