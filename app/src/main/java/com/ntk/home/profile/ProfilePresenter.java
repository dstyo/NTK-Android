package com.ntk.home.profile;

import com.ntk.data.ApiService;
import com.ntk.data.model.ChangePhoto;
import com.ntk.data.model.ReturnProfile;
import com.ntk.data.model.ReturnString;
import com.ntk.data.preference.PreferenceHelper;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dycode on 18/10/2016.
 */

public class ProfilePresenter {
    ProfileView profileView;
    ApiService apiService;

    public ProfilePresenter(ProfileView profileView, ApiService apiService){
        this.profileView = profileView;
        this.apiService = apiService;
    }

    public void getProfile(){
        profileView.showLoad();
        apiService.getProfile(PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnProfile>>) new Func1<Throwable, Observable<? extends ReturnProfile>>() {
                    @Override
                    public Observable<? extends ReturnProfile> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnProfile>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        profileView.showError(e.getMessage());
                        profileView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnProfile resource) {
                        profileView.showProfile(resource.getData());
                        profileView.dissmissLoad();


                    }
                });
    }

    public void changeProfile(String image){
        ChangePhoto changePhoto = new ChangePhoto();
        changePhoto.setPhoto(image);
        apiService.changePhotoProfile(PreferenceHelper.getUserData().getAccessToken(), changePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        profileView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getMeta().getCode()!=201) {
                            profileView.showError(returnString.getMeta().getMessage());
                        }
                    }
                });
    }
}
