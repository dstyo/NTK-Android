package com.ntk.home.profile;

import com.ntk.data.model.Profile;

/**
 * Created by Dycode on 18/10/2016.
 */

public interface ProfileView {
    void showLoad();
    void dissmissLoad();
    void showError(String string);
    void showProfile(Profile profile);
}
