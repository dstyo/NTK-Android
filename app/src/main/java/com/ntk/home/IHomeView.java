package com.ntk.home;

/**
 * Created by Dycode on 28/10/2016.
 */

public interface IHomeView {
    void showLoad();
    void showError(String error);
    void dissmissLoad();
    void showDialog(String idnews);
    void unRead(Boolean unread);
}
