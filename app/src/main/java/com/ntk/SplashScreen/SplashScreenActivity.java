package com.ntk.SplashScreen;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ntk.R;
import com.ntk.base.BaseActivity;
import com.ntk.data.preference.PreferenceHelper;
import com.ntk.home.HomeActivity;
import com.ntk.login.LoginActivity;
import com.ntk.utils.CommonUtils;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * @author Dwi Setiyono <dstyo91@gmail.com>
 * @since 2017.27.02
 */
public class SplashScreenActivity extends BaseActivity {

    GoogleCloudMessaging gcm;
    private String mobileDeviceId;
    static final String GOOGLE_PROJECT_ID = "1089418070668";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gcm = GoogleCloudMessaging.getInstance(this);

        gcm = GoogleCloudMessaging.getInstance(Activity);
        registerInBackground();

//        CommonUtils.showToast(this, mobileDeviceId);
    }

    @Override
    protected int getActivityView() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        changeColorSystemBar(R.color.white_quality_survey);

        if (CommonUtils.connectionAvailable(SplashScreenActivity.this) == false) {
            //Function.displayToast(SplashScreenActivity.this, "Please Check Your Internet Connectivity");
            new MaterialDialog.Builder(SplashScreenActivity.this)
                    .title(getString(R.string.dialog_error_conection))
                    .content(getString(R.string.dialog_error_conection_content))
                    .positiveText(getString(R.string.dialog_yes))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                    })
                    .show();
        } else {
            Thread timerThread = new Thread(){
                public void run(){
                    try{
                        sleep(2000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        if (PreferenceHelper.isLogin()) {
                            Intent intent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            return;
                        }
                        else {

                            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                            intent.putExtra("gcm", mobileDeviceId);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    }
                }
            };
            timerThread.start();
        }
    }
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    mobileDeviceId = gcm.register(GOOGLE_PROJECT_ID);
                    msg = mobileDeviceId;
                } catch (IOException ex) {
                    mobileDeviceId = ex.getMessage();
                    msg = mobileDeviceId;
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                mobileDeviceId = s;
            }
        }.execute(null, null, null);
    }
}
