package com.ntk.NewsDetail;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.base.BaseActivity;
import com.ntk.constant.UrlConstant;
import com.ntk.data.ApiService;
import com.ntk.data.model.Comment;
import com.ntk.data.preference.PreferenceHelper;
import com.ntk.utils.CustomRecycleView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dycode on 06/10/2016.
 */

public class NewsDetailActivity extends BaseActivity implements NewsDetailView {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.img_share)
    ImageView imgShare;
    @BindView(R.id.img_bookmark)
    ImageView imgBookmark;
    @BindView(R.id.layout_add_comment)
    LinearLayout layoutAddComment;
    @BindView(R.id.input_more)
    EditText inputMore;
    @BindView(R.id.layout_comment)
    LinearLayout layoutComment;
    @BindView(R.id.rv_comment)
    CustomRecycleView rvComment;
    @BindView(R.id.img_send_comment)
    TextView imgSendComment;
    @BindView(R.id.text_comment)
    TextView textComment;
    @BindView(R.id.layout_content)
    LinearLayout layoutContent;
    @BindView(R.id.text_more)
    TextView textMore;
    @BindView(R.id.progressBar)
    ProgressBar progress;
    private ProgressDialog progressDialog;
    String idNews = "";
    Boolean bookmark = false;

    @Inject
    ApiService apiService;
    NewsDetailRCAdapter newsDetailRCAdapter;
    NewsDetailPresenter newsDetailPresenter;
    int sizeComment = 3;

    List<Comment> commentList = new ArrayList<>();

    @Override
    protected int getActivityView() {
        return R.layout.activity_detail_news;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        layoutContent.setVisibility(View.GONE);
        textMore.setVisibility(View.GONE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
//        webView.setWebViewClient(new MyWebViewClient());

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started
                // Visible the progressbar
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url){
                // Do something when page loading finished
            }

        });

        webView.setWebChromeClient(new WebChromeClient(){

            public void onProgressChanged(WebView view, int newProgress){
                // Update the progress bar with page loading progress
                progress.setProgress(newProgress);
                if(newProgress == 100){
                    // Hide the progressbar
                    progress.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        ((QualityFeedbackApp) getApplication())
                .getComponent()
                .inject(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            idNews = bundle.getString("idNews");
            bookmark = bundle.getBoolean("bookmark");
        }

        if (bookmark) {
            imgBookmark.setImageResource(R.mipmap.favorite_grey_on);
        } else {
            imgBookmark.setImageResource(R.mipmap.favorite_grey_off);
        }
        newsDetailPresenter = new NewsDetailPresenter(apiService, this);
        newsDetailRCAdapter = new NewsDetailRCAdapter(this, this);

        webView.loadUrl(UrlConstant.ROOT + "api/v1/news/" + idNews + "?access_token=" + PreferenceHelper.getUserData().getAccessToken());
    }

    @OnClick(R.id.layout_add_comment)
    public void onAddComment() {
        layoutAddComment.setVisibility(View.GONE);
        layoutComment.setVisibility(View.VISIBLE);
        inputMore.requestFocus();
        inputMore.setCursorVisible(true);
    }

    @Override
    public void showLoad() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.dialog_progress));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void dissmissLoad() {
        progressDialog.dismiss();
    }

    @Override
    public void showError(String string) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(string)
                .show();

//        if (string.equals("HTTP 401 Unauthorized")) {
//            PreferenceHelper.setLogin(false);
//            PreferenceHelper.logout();
//            Intent intent = new Intent(this, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//            finish();
//        }
    }

    @Override
    public void showDataComment(List<Comment> comments) {
        layoutContent.setVisibility(View.VISIBLE);
        textComment.setText(getResources().getString(R.string.detail_comment) + " (" + comments.size() + ") ");
        commentList = comments;
        listComment();
    }

    private void listComment() {
        newsDetailRCAdapter.clear();
        if (sizeComment <= commentList.size()) {
            textMore.setVisibility(View.VISIBLE);
            for (int i = 0; i < sizeComment; i++) {
                newsDetailRCAdapter.add(commentList.get(i));
            }
        } else {
            textMore.setVisibility(View.GONE);
            for (int i = 0; i < commentList.size(); i++) {
                newsDetailRCAdapter.add(commentList.get(i));
            }
        }
        rvComment.setAdapter(newsDetailRCAdapter);
        rvComment.setUpAsList();
    }

    @Override
    public void onClickDelete(final Integer id) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.delete_comment))
                .content(getString(R.string.delete_comment_content))
                .positiveText(getString(R.string.delete_comment_yes))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        newsDetailPresenter.deleteComment(String.valueOf(id));
                    }
                })
                .negativeText(getString(R.string.delete_comment_no))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .show();
    }

    @Override
    public void onClickEdit(final Integer id, final String Comment) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_comment);
        TextView textEdit = ButterKnife.findById(dialog, R.id.text_edit);
        TextView textCancel = ButterKnife.findById(dialog, R.id.text_cancel);
        final EditText editComment = ButterKnife.findById(dialog, R.id.input_comment);
        editComment.setText(Comment);
        textEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editComment.getText().toString().equals(Comment) && editComment.length() != 0) {
                    dialog.dismiss();
                    newsDetailPresenter.editComment(String.valueOf(id), editComment.getText().toString());
                }
            }
        });
        textCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.img_back)
    public void onBack() {
        Intent intent=new Intent();
        intent.putExtra("refresh",true);
        setResult(2);
        finish();
    }

    @OnClick(R.id.img_send_comment)
    public void onClick() {
        if (inputMore.length() != 0) {
            newsDetailPresenter.sendComment(inputMore.getText().toString());
            inputMore.setText("");
            layoutAddComment.setVisibility(View.VISIBLE);
            layoutComment.setVisibility(View.GONE);
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        intent.putExtra("refresh",true);
        setResult(2);
        finish();
    }
    @OnClick(R.id.text_more)
    public void onLoad() {
        if (sizeComment < commentList.size()) {
            sizeComment = sizeComment + 3;
            listComment();
        }
    }


}
