package com.ntk.NewsDetail;

import com.ntk.data.model.Comment;

import java.util.List;

/**
 * Created by Dycode on 18/10/2016.
 */

public interface NewsDetailView {
    void showLoad();
    void dissmissLoad();
    void showError(String string);
    void showDataComment(List<Comment> comments);
    void onClickDelete(Integer id);
    void onClickEdit(Integer id, String Comment);
}
