package com.ntk.NewsDetail;

import com.ntk.data.ApiService;
import com.ntk.data.model.AddComment;
import com.ntk.data.model.Comment;
import com.ntk.data.model.ReturnAddComment;
import com.ntk.data.model.ReturnComment;
import com.ntk.data.model.ReturnString;
import com.ntk.data.preference.PreferenceHelper;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dycode on 18/10/2016.
 */

public class NewsDetailPresenter {
    ApiService apiService;
    NewsDetailView newsDetailView;
    List<Comment> commentList;
    String idNews ="";

    public NewsDetailPresenter(ApiService apiService, NewsDetailView newsDetailView){
        this.apiService = apiService;
        this.newsDetailView = newsDetailView;
    }

    public void getListComment(String id){
        newsDetailView.showLoad();
        idNews = id;
        apiService.getComment(id, PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnComment>>) new Func1<Throwable, Observable<? extends ReturnComment>>() {
                    @Override
                    public Observable<? extends ReturnComment> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnComment>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsDetailView.showError(e.getMessage());
                        newsDetailView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnComment resource) {
                        newsDetailView.showDataComment(resource.getData());
                        commentList = resource.getData();
                        newsDetailView.dissmissLoad();
                    }
                });
    }

    public void sendComment(String comment){
        newsDetailView.showLoad();
        AddComment addComment = new AddComment();
        addComment.setNewsId(Integer.parseInt(idNews));
        addComment.setComment(comment);
        apiService.addComment(PreferenceHelper.getUserData().getAccessToken(), addComment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnAddComment>>) new Func1<Throwable, Observable<? extends ReturnAddComment>>() {
                    @Override
                    public Observable<? extends ReturnAddComment> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnAddComment>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsDetailView.showError(e.getMessage());
                        newsDetailView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnAddComment resource) {
                        newsDetailView.dissmissLoad();
                        getListComment(idNews);
                    }
                });
    }

    public void deleteComment(String idComment){
        newsDetailView.showLoad();
        apiService.deleteComment(idComment, PreferenceHelper.getUserData().getAccessToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsDetailView.showError(e.getMessage());
                        newsDetailView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnString resource) {
                        newsDetailView.dissmissLoad();
                        getListComment(idNews);
                    }
                });
    }

    public void editComment(String idComment, String comment){
        newsDetailView.showLoad();
        AddComment addComment = new AddComment();
        addComment.setComment(comment);
        apiService.editComment(idComment, PreferenceHelper.getUserData().getAccessToken(), addComment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnAddComment>>) new Func1<Throwable, Observable<? extends ReturnAddComment>>() {
                    @Override
                    public Observable<? extends ReturnAddComment> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnAddComment>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        newsDetailView.showError(e.getMessage());
                        newsDetailView.dissmissLoad();
                    }

                    @Override
                    public void onNext(ReturnAddComment resource) {
                        newsDetailView.dissmissLoad();
                        getListComment(idNews);
                    }
                });
    }
}
