package com.ntk.NewsDetail;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.base.BaseItemRecyclerViewHolder;
import com.ntk.data.model.Comment;
import com.ntk.data.preference.PreferenceHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by Dycode on 19/10/2016.
 */

public class NewsDetailViewHolder extends BaseItemRecyclerViewHolder<Comment> {

    NewsDetailView newsDetailView;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_delete)
    ImageView imgDelete;
    @BindView(R.id.text_content)
    TextView textContent;
    @BindView(R.id.text_time)
    TextView textTime;

    public NewsDetailViewHolder(Context mContext, View itemView, BaseAdapterItemReyclerView.OnItemClickListener itemClickListener, BaseAdapterItemReyclerView.OnLongItemClickListener longItemClickListener, NewsDetailView newsDetailView) {
        super(mContext, itemView, itemClickListener, longItemClickListener);
        this.newsDetailView = newsDetailView;
    }

    @Override
    public void bind(final Comment comment) {
        Date newDate = null;
        textName.setText(comment.getUserData().getFullname());
        textContent.setText(comment.getComment());
        String date = comment.getUpdatedAt();
        String strCurrentDate = date.substring(0, 10);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("dd MMM, yyyy");
        String dates = format.format(newDate);
        textTime.setText(dates);

        if (comment.getUserData().getNik().equals(PreferenceHelper.getUserData().getUsername())){
            imgEdit.setVisibility(View.VISIBLE);
            imgDelete.setVisibility(View.VISIBLE);
        }
        else {
            imgEdit.setVisibility(View.GONE);
            imgDelete.setVisibility(View.GONE);
        }

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsDetailView.onClickEdit(comment.getId(), comment.getComment());
            }
        });
        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsDetailView.onClickDelete(comment.getId());
            }
        });
    }
}
