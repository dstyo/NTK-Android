package com.ntk.NewsDetail;

import android.content.Context;
import android.view.ViewGroup;

import com.ntk.R;
import com.ntk.base.BaseAdapterItemReyclerView;
import com.ntk.data.model.Comment;

/**
 * Created by Dycode on 19/10/2016.
 */

public class NewsDetailRCAdapter extends BaseAdapterItemReyclerView<Comment, NewsDetailViewHolder> {
    NewsDetailView newsDetailView;

    public NewsDetailRCAdapter(Context mContext, NewsDetailView newsDetailView) {
        super(mContext);
        this.newsDetailView = newsDetailView;
    }

    @Override
    protected int getItemView(int viewType) {
        return R.layout.item_comment;
    }

    @Override
    public NewsDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsDetailViewHolder(mContext, getView(parent, viewType), itemClickListener, longItemClickListener, newsDetailView);
    }
}
