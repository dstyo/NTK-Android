package com.ntk.verification;

import com.ntk.data.ApiService;
import com.ntk.data.model.InputNik;
import com.ntk.data.model.Login;
import com.ntk.data.model.ReturnLogin;
import com.ntk.data.model.ReturnString;
import com.ntk.data.model.UpdateDeviceId;
import com.ntk.data.preference.PreferenceHelper;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public class VerificationPresenter {
    private IVerificationView iVerificationView;
    private ApiService apiService;
    String deviceId, accessToken;

    public VerificationPresenter(IVerificationView iVerificationView, ApiService apiService, String deviceId){
        this.iVerificationView = iVerificationView;
        this.apiService = apiService;
        this.deviceId = deviceId;
    }

    public void checkCode(String verifikasi, final String NIK){
        iVerificationView.showLoad();
        InputNik inputNik = new InputNik();
        inputNik.setNik(NIK);
        inputNik.setCode(verifikasi);
        apiService.verificationCode(inputNik)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iVerificationView.showError(e.getMessage());
                        iVerificationView.dismissLoad();
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getData().equals("CODE VALID")) {
                            login(NIK);
                        }
                        else {
                            iVerificationView.showError(returnString.getData());
                            iVerificationView.dismissLoad();
                        }

                    }
                });
    }
    private void login(final String nik){
        Login login = new Login();
        login.setUsername(nik);
        login.setGrantType("password");
        login.setPassword("$2a$08$eSR5C8RQojS9gktm1QXkduYLkehux16o51VtAzjN5.OYRewES2W0m");
        login.setScope("*");
        apiService.login(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnLogin>>) new Func1<Throwable, Observable<? extends ReturnLogin>>() {
                    @Override
                    public Observable<? extends ReturnLogin> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnLogin>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iVerificationView.dismissLoad();
                        iVerificationView.showError(e.getMessage());

                    }

                    @Override
                    public void onNext(ReturnLogin returnLogin) {
                        accessToken = returnLogin.getAccessToken();
                        updateDeviceId(nik);
                    }
                });
    }

    private void updateDeviceId(final String nik){
        UpdateDeviceId updateDeviceId = new UpdateDeviceId();
        updateDeviceId.setToken(deviceId);
        updateDeviceId.setClient(1);
        apiService.deviceId(accessToken, updateDeviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext((Func1<Throwable, ? extends Observable<? extends ReturnString>>) new Func1<Throwable, Observable<? extends ReturnString>>() {
                    @Override
                    public Observable<? extends ReturnString> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ReturnString>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        iVerificationView.showError(e.getMessage());
                        iVerificationView.dismissLoad();
                    }

                    @Override
                    public void onNext(ReturnString returnString) {
                        if (returnString.getMeta().getCode()==200) {
                            iVerificationView.nextPage(accessToken);
                            PreferenceHelper.setLogin(true);
                            PreferenceHelper.saveUserData(accessToken, nik);
                            iVerificationView.dismissLoad();
                        }
                        else {
                            iVerificationView.showError(returnString.getData());
                            iVerificationView.dismissLoad();
                        }

                    }
                });
    }
}
