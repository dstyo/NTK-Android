package com.ntk.verification;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.ntk.QualityFeedbackApp;
import com.ntk.R;
import com.ntk.base.BaseActivity;
import com.ntk.data.ApiService;
import com.ntk.data.gcm.RealmGcm;
import com.ntk.home.HomeActivity;
import com.rengwuxian.materialedittext.MaterialEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dean Nurchusnul on 14/09/2016.
 */
public class VerificationActivity extends BaseActivity implements IVerificationView {

    CountDownTimer timer;
    BroadcastReceiver broadcastReceiver;
    ProgressDialog progressDialog;
    private VerificationPresenter verificationPresenter;
    @Inject
    ApiService apiService;
    String nik, deviceId;

    @BindView(R.id.text_count)
    TextView textCount;
    @BindView(R.id.input_code)
    MaterialEditText inputCode;
    @BindView(R.id.button_login)
    MaterialRippleLayout buttonLogin;
    @BindView(R.id.cardView)
    CardView cardView;

    RealmGcm realmGcm = new RealmGcm();

    @Override
    protected int getActivityView() {
        return R.layout.activity_verification;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        changeColorSystemBar(R.color.background_grey_quality_survey);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ((QualityFeedbackApp) getApplication())
                .getComponent()
                .inject(this);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            nik = bundle.getString("nik");
            deviceId = bundle.getString("gcm");
            timer();
            verivicationCode();

            if (realmGcm.localeSave() != null && deviceId == null){
                String id = realmGcm.localeSave().getGcmCode();
                deviceId = id;
            }
            else {
                realmGcm.saveGCM(deviceId);
            }
            setupPresenter();
        }
    }

    @OnClick(R.id.button_login)
    public void onClick() {
        if (inputCode.length() == 0) {
            inputCode.setError(getResources().getString(R.string.login_null));
        } else {
            verificationPresenter.checkCode(inputCode.getText().toString(), nik);
        }
    }

    public void timer() {
        timer = new CountDownTimer(59000, 1000) {

            public void onTick(long millisUntilFinished) {
                Long second = millisUntilFinished / 1000;
                textCount.setText(getString(R.string.verification_count, second));
            }

            public void onFinish() {
                textCount.setVisibility(View.GONE);
            }
        };
        timer.start();
    }

    public void verivicationCode() {
        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getStringExtra("get_msg");

                String body = msg.substring(msg.lastIndexOf(":") + 1, msg.length());
                String phoneNumber[] = msg.split(":");
                String activationCode = body.replace(getResources().getString(R.string.sms_content), "").replaceAll("\\s+", "");

                if (phoneNumber[0].equals(getResources().getString(R.string.client_number_one)) || phoneNumber[0].equals(getResources().getString(R.string.client_number_two))
                        || phoneNumber[0].equals(getResources().getString(R.string.client_number_three)) || phoneNumber[0].equals(getResources().getString(R.string.client_number_four))) {
                    textCount.setVisibility(View.GONE);
                    timer.cancel();
                    inputCode.setText(activationCode);

                }
            }
        };
        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void showLoad() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.dialog_progress));
        progressDialog.show();
    }

    @Override
    public void dismissLoad() {
        progressDialog.dismiss();
    }

    @Override
    public void nextPage(String accessToken) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("accessToken", accessToken);
        intent.putExtra("notif", "home");
        startActivity(intent);
        finish();
    }

    @Override
    public void showError(String error) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.dialog_error_title))
                .content(error)
                .show();
    }
    private void setupPresenter() {
        if (verificationPresenter == null) {
            verificationPresenter = new VerificationPresenter(this, apiService, deviceId);
        }
    }
}
