package com.ntk.verification;

/**
 * Created by Dean Nurchusnul on 16/09/2016.
 */
public interface IVerificationView {
    void showLoad();
    void dismissLoad();
    void nextPage(String accessToken);
    void showError(String error);
}
